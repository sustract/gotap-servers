/*
 * Autor @CarloOSX
 */
/*Definimos el modulo al que pertenecemos*/
angular.module('Gotap')
//My Class
        /*Definimos el controlador que vamos a usar*/
        .controller('TransaccionesController', function (IonicFunctions, /*Starting with Angular injection*/ $scope, /* nfcService,*/ $filter) {
            /**
             * Variables globales y objetos
             */
            /*declaramos el objeto ionic con el que podremos acceder a los "métodos" definidas en su clase*/
            utils = new IonicFunctions();
            var JSONObject = {"idUsuario": localStorage.idUsuario, "password": localStorage.password};
            utils.postHttp("gotapGetTransactions", JSONObject).then(function (resolvedVal) {
                if (resolvedVal.status === 500) {
                    console.log(resolvedVal.status);
                } else {
                    console.log(resolvedVal.data);
                    $scope.transactions = resolvedVal.data;
                }
            });
$scope.getTransactions = function(){
            var JSONObject = {"idUsuario": localStorage.idUsuario, "password": localStorage.password};
            utils.postHttp("gotapGetTransactions", JSONObject).then(function (resolvedVal) {
                if (resolvedVal.status === 500) {
                    console.log(resolvedVal.status);
                } else {
                    console.log(resolvedVal.data);
                    $scope.transactions = resolvedVal.data;
                }
            });
        };

            $scope.formatDate = function (fecha) {
                if (fecha) {
                    Number.prototype.padLeft = function (base, chr) {
                        var len = (String(base || 10).length - String(this).length) + 1;
                        return len > 0 ? new Array(len).join(chr || '0') + this : this;
                    };
                    var d = new Date(fecha),
                            dformat = [
                                d.getDate().padLeft(),
                                (d.getMonth() + 1).padLeft(),
                                d.getFullYear()].join('/') +
                            ' ' +
                            [d.getHours().padLeft(),
                                d.getMinutes().padLeft(),
                                d.getSeconds().padLeft()].join(':');
                    return dformat;
                }
            };

        });