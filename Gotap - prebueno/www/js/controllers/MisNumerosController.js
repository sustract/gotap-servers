/*
 * Autor @CarloOSX
 */
/*Definimos el modulo al que pertenecemos*/
angular.module('Gotap')
//My Class
        /*Definimos el controlador que vamos a usar*/
        .controller('MisNumerosController', function (IonicFunctions, /*Starting with Angular injection*/ $scope, /* nfcService,*/ $filter) {
            /**
             * Variables globales y objetos
             */
            /*declaramos el objeto ionic con el que podremos acceder a los "métodos" definidas en su clase*/
            utils = new IonicFunctions();

            var JSONObject = {"idUsuario": localStorage.idUsuario, "password": localStorage.password};
            utils.postHttp("gotapGetTelefonos", JSONObject).then(function (resolvedVal) {
                if (resolvedVal.status === 500) {
                    console.log(resolvedVal.status);
                } else {
                    $scope.telefonos = resolvedVal.data;
                }
            });
            
           $scope.deleteTelefono = function (numero) {

           var telefono =  $scope.telefonos.filter(function (telefono) { return telefono.numero === numero; });
   
                var JSONObject = {"id": telefono[0].id, "numero": parseInt(telefono[0].numero), "alias":telefono[0].alias,"company": "kolbi", "estado": "inactivo", "idUsuario": localStorage.idUsuario, "password": localStorage.password};
              
                utils.postHttp("gotapUpdateTelefono", JSONObject).then(function (resolvedVal) {

                    if (resolvedVal.status === 500) {

                        utils.alertError('Error eliminando telefono');

                    } else {

                       location.reload();
                    }
                });

            };
            
                    $scope.addTelefono = function (movil, alias) {

     
   
                var JSONObject = {"numero": movil, "alias":alias,"company": " ", "favorito": "si", "idUsuario": localStorage.idUsuario, "password": localStorage.password};
        
                utils.postHttp("gotapInsertTelefono", JSONObject).then(function (resolvedVal) {

                    if (resolvedVal.status === 500) {

                        utils.alertError('Error al añadir telefono telefono');

                    } else {

                       location.reload();
                    }
                });

            };


        });