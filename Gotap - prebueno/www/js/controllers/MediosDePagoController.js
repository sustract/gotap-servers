/*
 * Autor @CarloOSX
 */
/*Definimos el modulo al que pertenecemos*/
angular.module('Gotap')

        /*Definimos el controlador que vamos a usar*/
        .controller('MediosDePagoController', function (IonicFunctions, /*Starting with Angular injection*/ $scope, $state) {
            /**
             * Variables globales y objetos
             */

            /*declaramos el objeto ionic con el que podremos acceder a los "métodos" definidas en su clase*/
            utils = new IonicFunctions();

            var JSONObject = {"idUsuario": localStorage.idUsuario, "password": localStorage.password};
            var jsonData = JSON.stringify(JSONObject);


            utils.postHttp("gotapGetTarjetas", JSONObject).then(function (resolvedVal) {

                if (resolvedVal.status === 500) {

                    utils.alertError('Error al importar tarjetas');

                } else {

                    $scope.tarjetas = resolvedVal.data;
                }
            });


            $scope.addTarjeta = function (numero, nombre, mes, ano, cvc) {

                var EsValido;
                var tipoTarjeta;
                var añoActual = new Date().getFullYear();

                if (!numero) {
                    utils.alertError("Introduce el número de tarjeta");
                } else if (!nombre) {
                    utils.alertError("Introduce el nombre del titular de la tarjeta");

                } else if (!ano) {
                    utils.alertError("Introduce un año de expiración válido");

                } else if (!mes) {
                    utils.alertError("Introduce un mes de expiración válido");

                } else if (!cvc) {
                    utils.alertError("Introduce un cvc válido");

                } else {

                    //Antes de nada con una Funcion de Jquery que implementa el algoritmo de Luhn miramos si la tarjeta es valida : https://github.com/PawelDecowski/jQuery-CreditCardValidator/
                    $('#validarTarjeta').validateCreditCard(function (result) {
                        if (result.valid === true && result.length_valid === true && result.luhn_valid === true) {
                            console.log(result.card_type.name);
                            EsValido = true;
                            tipoTarjeta = result.card_type.name;
                        } else {
                            EsValido = false;
                        }
                    });

                    console.log(tipoTarjeta);

                    var anyExpiracion = parseInt("20" + ano);

                    if (!EsValido || nombre.length === 0 || mes.length < 2 || ano.length < 2 || cvc.length < 3 || anyExpiracion < añoActual) {
                        utils.alertError("Esta tarjeta no es válida \n ¡Por favor introduce una que lo sea!");

                    } else {
                        var noms = nombre.split(" ");
                        var fechaNueva = new Date(anyExpiracion, mes, 00);
                        console.log(fechaNueva);
                        var fechita = fechaNueva.getFullYear()+"-"+parseInt(fechaNueva.getMonth()+1)+"-"+fechaNueva.getDate();
                        var JSONObject = {"numero": numero, "nombrePropietario": noms[0], "apellidoPropietario": noms[1], "cvc": cvc, "caducidad": fechita, "tipo": tipoTarjeta, "estado": "activa", "idUsuario": localStorage.idUsuario, "password": localStorage.password};
                        console.log(JSONObject);
                        utils.postHttp("gotapInsertTarjeta", JSONObject).then(function (resolvedVal) {

                            if (resolvedVal.status === 500) {

                                utils.alertError('¡No ha sido posible añadir su tarjeta!');

                            } else {


     location.reload();

                            }
                        });
                    }
                }
                ;

            };

            $scope.getMonth = function (mes) {

                var d = new Date(mes);
                return d.getMonth() + 1;

            };
            $scope.getYear = function (año) {
                var d = new Date(año);
                return d.getFullYear();
            };

            $scope.eliminarTarjeta = function (numero) {

           var tarjeta =  $scope.tarjetas.filter(function (tarjeta) { return tarjeta.numero === numero; });
             
                var JSONObject = {"id": tarjeta[0].id, "estado": "inactiva", "idUsuario": localStorage.idUsuario, "password": localStorage.password};

                utils.postHttp("gotapUpdateTarjeta", JSONObject).then(function (resolvedVal) {

                    if (resolvedVal.status === 500) {

                        utils.alertError('Error eliminando tarjeta');

                    } else {

                       location.reload();
                    }
                });

            };


        });
