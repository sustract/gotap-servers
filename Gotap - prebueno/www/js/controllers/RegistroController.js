/*
 * Autor @CarloOSX
 */

/* global Function, constructor, angular para poder hacer extends si es necesario */

Function.prototype.extends = function (superclass) {
    var constructor = this.prototype.constructor;
    this.prototype = Object.create(superclass.prototype);
    this.prototype.constructor = constructor;
    return this;
};

/*Definimos el modulo al que pertenecemos*/
angular.module('Gotap')

        /*Definimos el controlador que vamos a usar*/
        .controller('RegistroController', function (IonicFunctions, /*Starting with Angular injection*/ $scope, $state) {
            /**
             * Variables globales y objetos
             */

            /*declaramos el objeto ionic con el que podremos acceder a los "métodos" definidas en su clase*/
            utils = new IonicFunctions();
            $scope.utils = utils;

            $scope.provincias = [{provincia: "San José"}, {provincia: "Alajuela"}, {provincia: "Cartago"}, {provincia: "Heredia"}, {provincia: "Limón"}, {provincia: "Guanacaste"}, {provincia: "Puntarenas"}];

            /*---------------------------------------------Funciones para el Registro y Login---------------------------------------------*/

            //obtenemos el array de cantones en funcion de la provincia
            $scope.getCantones = function (provincia) {

                switch (provincia) {
                    case "San José":
                        $scope.cantones = [{canton: "San José"}, {canton: "Escazú"}, {canton: "Desamparados"}, {canton: "Puriscal"}, {canton: "Tarrazú"}, {canton: "Aserrí"}, {canton: "Mora"}, {canton: "Goicoechea"}, {canton: "Santa Ana"}, {canton: "Alajuelita"}, {canton: "Vásquez de Coronado"}, {canton: "Acosta"}, {canton: "Tibás"}, {canton: "Moravia"}, {canton: "Montes de Oca"}, {canton: "Turrubares"}, {canton: "Dota"}, {canton: "Curridabat"}, {canton: "Pérez Zeledón"}, {canton: "León Cortés Castro"}];
                        break;
                    case "Alajuela":
                        $scope.cantones = [{canton: "Alajuela"}, {canton: "San Ramón"}, {canton: "Grecia"}, {canton: "San Mateo"}, {canton: "Atenas"}, {canton: "Naranjo"}, {canton: "Palmares"}, {canton: "Poás"}, {canton: "Orotina"}, {canton: "San Carlos"}, {canton: "Zarcero"}, {canton: "Valverde Vega"}, {canton: "Upala"}, {canton: "Los Chiles"}, {canton: "Guatuso"}];
                        break;
                    case "Cartago":
                        $scope.cantones = [{canton: "Cartago"}, {canton: "Paraíso"}, {canton: "La unión"}, {canton: "Jimenez"}, {canton: "Turriabla"}, {canton: "Alvarado"}, {canton: "Oreamuno"}, {canton: "El Guarco"}];
                        break;
                    case "Heredia":
                        $scope.cantones = [{canton: "Heredia"}, {canton: "Barva"}, {canton: "Santo Domingo"}, {canton: "Santa Bárbara"}, {canton: "San Rafael"}, {canton: "San Isidro"}, {canton: "Belén"}, {canton: "Flores"}, {canton: "San Pablo"}, {canton: "Sarapiquí"}];
                        break;
                    case "Limón":
                        $scope.cantones = [{canton: "Limón"}, {canton: "Pococí"}, {canton: "Siquirres"}, {canton: "Talamanca"}, {canton: "Matina"}, {canton: "Guácimo"}];
                    case "Guanacaste":
                        $scope.cantones = [{canton: "Liberia"}, {canton: "Nicoya"}, {canton: "Santa Cruz"}, {canton: "Bagaces"}, {canton: "Carrillo"}, {canton: "Cañas"}, {canton: "Abangares"}, {canton: "Tilarán"}, {canton: "Nandayure"}, {canton: "La Cruz"}, {canton: "Hojancha"}];
                    case "Puntarenas":
                        $scope.cantones = [{canton: "Puntarenas"}, {canton: "Esparza"}, {canton: "Buenos Aires"}, {canton: "Montes de Oro"}, {canton: "Osa"}, {canton: "Quepos"}, {canton: "Golfito"}, {canton: "Coto Brus"}, {canton: "Parrita"}, {canton: "Corredores"}, {canton: "Garabito"}];
                        break;
                    default:
                }
            };


            /*Funcion para realizar registro*/
            $scope.userRegistration = function (nombre, apellido, apellido2, email, identificador, provincia, canton, nombreUsuario, password, password2) {
                /*Realizamos las comprobaciones necesarias*/
                var alphaExp = /^[a-zA-Z]+$/;

                if (!nombre) {
                    utils.alertError('Por favor, añade un nombre');

                } else if (!nombre.match(alphaExp)) {
                    utils.alertError('El nombre no puede contener números o espacios');

                } else if (!apellido) {
                    utils.alertError('Por favor, añade un apellido');

                } else if (!apellido.match(alphaExp)) {
                    utils.alertError('El apellido no puede contener números o espacios');

                } else if (!email) {
                    utils.alertError('Por favor, revisa la dirección de correo introducida');

                } else if (!identificador) {
                    utils.alertError('Por favor, añade un identificacor fiscal');

                } else if (identificador.length < 5) {
                    utils.alertError('El identificador fiscal debe tener como mínimo 5 caracteres');

                } else if (identificador.indexOf(" ") > -1) {
                    utils.alertError('El identificador no puede contener espacios');

                } else if (!provincia) {
                    utils.alertError('Por favor, añade una provincia');

                } else if (!canton) {
                    utils.alertError('Por favor, añade un cantón');

                } else if (!nombreUsuario) {
                    utils.alertError('Por favor, añade un nombre de usuario');

                } else if (nombreUsuario.length < 2) {
                    utils.alertError('El nombre de usuario debe tener como mínimo 2 caracteres');
                } else if (nombreUsuario.indexOf(" ") > -1) {
                    utils.alertError('El nombre de usuario no puede contener espacios');

                } else if (!password) {
                    utils.alertError('Por favor, añade una contraseña');

                } else if (password.length < 6) {
                    utils.alertError('El password debe tener como mínimo 6 caracteres');

                } else if (password.indexOf(" ") > -1) {
                    utils.alertError('El password no puede contener espacios');

                } else if (password !== password2) {
                    utils.alertError('l password debe ser igual en los dos campos');

                }

                var JSONObject = {"nombre": nombre, "apellido": apellido, "apellido2": apellido2, "identificador": identificador, "email": email, "usuario": nombreUsuario, "password": password, "estado": "activo", "uuidDevice": utils.getUUID(), "pais": "Costa Rica", "provincia": provincia, "canton": canton};

                utils.postHttp("gotapUserRegistration", JSONObject).then(function (resolvedVal) {

                    if (resolvedVal.status === 500) {

                        utils.alertError(resolvedVal.data);

                    } else {
                        utils.alertSuccess("Gracias por Registrarte <br> Ya puedes iniciar sesión");
                        $state.go('login');
                    }
                });


            };

        });
