/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('Gotap')

        .factory('IonicFunctions', function ($http, $cordovaDevice, $ionicPopup, $cordovaVibration, $timeout, $state, $window) {//My Class
            /**
             * Constructor, with class name
             */
            function IonicFunctions() {
                // Public properties, assigned to the instance ('this')
            }

            /*Definimos la direccion http del server*/
            var GoTapApi = "http://appsrv01.sevenandseven.net:28081/GoTapWS/";
            var login = false;
            /**
             * Public method, assigned to prototype
             */

            /*Cogemos el uddid del telefono con el plugin device*/
            /*Se llama automaticamente ondeviceReady*/
            IonicFunctions.prototype.getUUID = function () {
                var uuid = "null";
                document.addEventListener("deviceready", function () {
                    uuid = $cordovaDevice.getUUID();
                }, false);

                return uuid;
            };

            /* Funcion generica para devolver las respuestas del servidor pasandole unos argumentos */
            IonicFunctions.prototype.postHttp = function (ruta, JSONObject) {

                var jsonData = JSON.stringify(JSONObject);

                return $http.post(GoTapApi + ruta, jsonData).
                        then(function (response) {
                            return response;

                        }, function (response) {
                            return response;
                        });
            };

            /* Funcion generica para devolver las respuestas del servidor pasandole unos argumentos */
            IonicFunctions.prototype.formatDate = function (fecha) {
                if (fecha) {
                    Number.prototype.padLeft = function (base, chr) {
                        var len = (String(base || 10).length - String(this).length) + 1;
                        return len > 0 ? new Array(len).join(chr || '0') + this : this;
                    };
                    var d = new Date(fecha),
                            dformat = [
                                d.getDate().padLeft(),
                                (d.getMonth() + 1).padLeft(),
                                d.getFullYear()].join('/');
                    return dformat;
                }
            };
             
            /* Funcion generica para redireccionar entre pantallas, se le pasa la pantalla como argumento */
            IonicFunctions.prototype.goToPage = function (pantalla) {
                document.location = '#/' + pantalla;
            };

            /* Funcion generica para abrir urls externas */
            IonicFunctions.prototype.openWeb = function (link) {
                $window.open(link, '_self', 'location=yes');
            };
            
            /* Funcion generica para pintar alertas de éxito */
            IonicFunctions.prototype.alertSuccess = function (mensaje) {

                var alertPopup = $ionicPopup.alert({
                    title: '¡Enhorabuena!',
                    template: mensaje,
                    buttons: [
                        {
                            text: '<b>Aceptar</b>',
                            type: 'button-gotap',
                            onTap: function () {
                                alertPopup.close();
                            }
                        }
                    ]
                });
                $timeout(function () {
                    alertPopup.close();
                }, 3000);

            };
            /* Funcion generica para pintar alertas de error */
            IonicFunctions.prototype.alertError = function (mensaje) {

                var alertPopup = $ionicPopup.alert({
                    title: 'Atención',
                    template: mensaje,
                    buttons: [
                        {
                            text: '<b>Aceptar</b>',
                            type: 'button-gotap',
                            onTap: function () {
                                alertPopup.close();
                            }
                        }
                    ]
                });
                $cordovaVibration.vibrate(300);
                $timeout(function () {
                    alertPopup.close();
                }, 3000);

            };
            
            /**
             * Return the constructor function
             */
            return IonicFunctions;
        });
