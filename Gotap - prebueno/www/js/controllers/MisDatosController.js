/*
 * Autor @CarloOSX
 */
/*Definimos el modulo al que pertenecemos*/
angular.module('Gotap')
//My Class
        /*Definimos el controlador que vamos a usar*/
        .controller('MisDatosController', function (IonicFunctions, $scope, $ionicPopup, $ionicActionSheet, $timeout, $state) {

            /*
             *  Variables globales y objetos
             */
            $scope.provincias = [{provincia: "San José"}, {provincia: "Alajuela"}, {provincia: "Cartago"}, {provincia: "Heredia"}, {provincia: "Limón"}, {provincia: "Guanacaste"}, {provincia: "Puntarenas"}];

            $scope.getCantones = function (provincia) {
                switch (provincia) {
                    case "San José":
                        $scope.cantones = [{canton: "San José"}, {canton: "Escazú"}, {canton: "Desamparados"}, {canton: "Puriscal"}, {canton: "Tarrazú"}, {canton: "Aserrí"}, {canton: "Mora"}, {canton: "Goicoechea"}, {canton: "Santa Ana"}, {canton: "Alajuelita"}, {canton: "Vásquez de Coronado"}, {canton: "Acosta"}, {canton: "Tibás"}, {canton: "Moravia"}, {canton: "Montes de Oca"}, {canton: "Turrubares"}, {canton: "Dota"}, {canton: "Curridabat"}, {canton: "Pérez Zeledón"}, {canton: "León Cortés Castro"}];
                        break;
                    case "Alajuela":
                        $scope.cantones = [{canton: "Alajuela"}, {canton: "San Ramón"}, {canton: "Grecia"}, {canton: "San Mateo"}, {canton: "Atenas"}, {canton: "Naranjo"}, {canton: "Palmares"}, {canton: "Poás"}, {canton: "Orotina"}, {canton: "San Carlos"}, {canton: "Zarcero"}, {canton: "Valverde Vega"}, {canton: "Upala"}, {canton: "Los Chiles"}, {canton: "Guatuso"}];
                        break;
                    case "Cartago":
                        $scope.cantones = [{canton: "Cartago"}, {canton: "Paraíso"}, {canton: "La unión"}, {canton: "Jimenez"}, {canton: "Turriabla"}, {canton: "Alvarado"}, {canton: "Oreamuno"}, {canton: "El Guarco"}];
                        break;
                    case "Heredia":
                        $scope.cantones = [{canton: "Heredia"}, {canton: "Barva"}, {canton: "Santo Domingo"}, {canton: "Santa Bárbara"}, {canton: "San Rafael"}, {canton: "San Isidro"}, {canton: "Belén"}, {canton: "Flores"}, {canton: "San Pablo"}, {canton: "Sarapiquí"}];
                        break;
                    case "Limón":
                        $scope.cantones = [{canton: "Limón"}, {canton: "Pococí"}, {canton: "Siquirres"}, {canton: "Talamanca"}, {canton: "Matina"}, {canton: "Guácimo"}];
                    case "Guanacaste":
                        $scope.cantones = [{canton: "Liberia"}, {canton: "Nicoya"}, {canton: "Santa Cruz"}, {canton: "Bagaces"}, {canton: "Carrillo"}, {canton: "Cañas"}, {canton: "Abangares"}, {canton: "Tilarán"}, {canton: "Nandayure"}, {canton: "La Cruz"}, {canton: "Hojancha"}];
                    case "Puntarenas":
                        $scope.cantones = [{canton: "Puntarenas"}, {canton: "Esparza"}, {canton: "Buenos Aires"}, {canton: "Montes de Oro"}, {canton: "Osa"}, {canton: "Quepos"}, {canton: "Golfito"}, {canton: "Coto Brus"}, {canton: "Parrita"}, {canton: "Corredores"}, {canton: "Garabito"}];
                        break;
                    default:
                }
            };

            /*declaramos el objeto ionic con el que podremos acceder a los "métodos" definidas en su clase*/
            utils = new IonicFunctions();
            $scope.telefono = localStorage.numeroRegistro;
            $scope.idUsuario = localStorage.idUsuario;
            $scope.nombre = localStorage.nombre;
            $scope.apellido = localStorage.apellido;
            $scope.apellido2 = localStorage.apellido2;
            $scope.identificador = localStorage.identificador;
            $scope.email = localStorage.email;
            $scope.usuario = localStorage.usuario;
            $scope.realpassword = localStorage.password;
            $scope.password = localStorage.password.replace(/./g, '•');//CUANDO SE ESTE PROBANDO ESTA PANTALLA NO REFRESCAR PORQUE SE PIERDE LA CONTRA !
            $scope.fechaRegistro = localStorage.fechaRegistro;
            $scope.provincia = localStorage.provincia;
            console.log($scope.provincia);
            $scope.getCantones($scope.provincia);
            $scope.canton = localStorage.canton;

            /*---------------------------------------------Funciones para la navegación---------------------------------------------*/
            $scope.navegacion = function (posicion) {

                utils.changePage(posicion);

            };

            $scope.alertBaja = function (contenido) {

                var alertPopup = $ionicPopup.alert({
                    title: '¡Adiós!',
                    template: contenido,
                    buttons: [
                        {
                            text: '<b>Aceptar</b>',
                            type: 'button-gotap',
                            onTap: function () {
                                alertPopup.close();
                                document.location = "#/login";
                            }
                        }
                    ]
                });
                $timeout(function () {
                    alertPopup.close();
                    document.location = "#/login";
                }, 4000);
            };

            /*---------------------------------------------Funciones Para la edicion de los datos---------------------------------------------*/

            $scope.showActionsheet = function (numero) {

                switch (numero) {
                    case 1:
                        elemento = ' nombre';
                        $scope.showSheet(elemento);
                        label = 1;
                        break;
                    case 2:
                        elemento = ' apellido';
                        $scope.showSheet(elemento);
                        label = 2;
                        break;
                    case 3:
                        elemento = ' segundo apellido';
                        $scope.showSheet(elemento);
                        label = 3;
                        break;
                    case 4:
                        utils.alertError('¡El identificador no se puede cambiar!');
                        break;
                    case 5:
                        elemento = ' email ';
                        $scope.showSheet(elemento);
                        label = 5;
                        break;
                    case 6:
                        elemento = ' Usuario';
                        $scope.showSheet(elemento);
                        label = 6;
                        break;
                    case 7:
                        elemento = ' Password';
                        $scope.showSheet(elemento);
                        label = 7;
                        break;
                    case 8:
                        utils.alertError('¡La fecha de registro no se puede cambiar!');
                        label = 8;
                        break;
                    case 9:
                        utils.alertError('¡Los números deben gestionarse desde el menú de teléfonos!');
                        //  label=9;
                        break;
                    case 10:
                        var hideSheet = $ionicActionSheet.show({
                            titleText: '¿Seguro que quieres darte de baja?',
                            buttons: [
                                {text: '<b><i class="icon ion-edit ion-trash-a assertive"></i></b> <b>¡Dame de baja!</b>'},
                                {text: '<i class="icon ion-ios-close-outline gotap"></i> Cancelar'}
                            ],
                            buttonClicked: function (index) {
                                console.log('BUTTON CLICKED', index);
                                if (index === 0) {
                                    JSONObject = {"nombre": $scope.nombre, "apellido": $scope.apellido, "apellido2": $scope.apellido2, "email": localStorage.email, "usuario": localStorage.usuario, "password": localStorage.password, "provincia": $scope.provincia, "canton": $scope.canton, "idUsuario": localStorage.idUsuario, "passwordUser": localStorage.password, "estado": "inactivo"};

                                    utils.postHttp("gotapUserChange", JSONObject).then(function (resolvedVal) {

                                        if (resolvedVal.status === 500) {

                                            utils.alertError(resolvedVal.data);

                                        } else {
                                            utils.alertSuccess("Tu baja ha sido realizada");
                                            console.log(resolvedVal.data);
                                            // $state.go('login');
                                        }
                                    });

                                } else {
                                    hideSheet();
                                }
                            }
                        });
                        $timeout(function () {
                            hideSheet();
                        }, 4000);
                }
            };

            $scope.showSheet = function (elemento) {


                var hideSheet = $ionicActionSheet.show({
                    titleText: '¿Deseas cambiar tu' + elemento + '?',
                    buttons: [
                        {text: '<i class="icon ion-edit custom-icon-sheet"></i> Editar'},
                        {text: '<i class="icon ion-ios-close-outline assertive"></i> Cancelar'}
                    ],
                    buttonClicked: function (index) {
                        console.log('BUTTON CLICKED', index);

                        if (index === 0) {

                            $scope.doEdit();
                            hideSheet();

                        } else {
                            hideSheet();
                        }
                    }

                });
                $timeout(function () {
                    hideSheet();
                }, 4000);
            };

            $scope.doEdit = function () {
                $scope.data = {};
                // An elaborate, custom popup
                var myPopup = $ionicPopup.show({
                    template: '<input type="text" ng-model="data.info">',
                    title: 'Introduce tu nuevo' + elemento,
                    //subTitle: nombre,
                    scope: $scope,
                    buttons: [
                        {text: 'Cancelar'},
                        {
                            text: '<b>Guardar</b>',
                            type: 'button-balanced',
                            onTap: function (e) {
                                if (!$scope.data.info) {
                                    //don't allow the user to close unless he enters wifi password
                                    e.preventDefault();
                                } else {
                                    console.log(label);
                                    switch (label) {


                                        case 1:
                                            $scope.nombre = $scope.data.info;
                                            break;
                                        case 2:
                                            $scope.apellido = $scope.data.info;
                                            break;
                                        case 3:
                                            $scope.apellido2 = $scope.data.info;
                                            console.log($scope.apellido2);
                                            break;
                                        case 5:
                                            $scope.email = $scope.data.info;
                                            break;
                                        case 6:
                                            $scope.usuario = $scope.data.info;
                                            break;
                                        case 7:
                                            $scope.userPass = $scope.data.info;
                                            console.log($scope.userPass);
                                            break;

                                        default:
                                            break;
                                    }
                                    $scope.updateUser();

                                }

                            }
                        }
                    ]
                });

            };

            $scope.updateUser = function () {


                //Realizamos todas las comprobaciones de los datos introducidos
                var alphaExp = /^[a-zA-Z]+$/;

                if (!$scope.nombre.match(alphaExp)) {
                    utils.alertError('¡El nombre no puede contener números o espacios!');
                    return;
                } else if (!$scope.apellido) {
                    utils.alertError('¡El apellido no puede estar vacío!');
                    return;
                } else if (!$scope.apellido.match(alphaExp)) {
                    utils.alertError('¡El apellido no puede contener números o espacios!');
                    return;
                } else if (!$scope.email) {
                    utils.alertError('El correo no puede estar vacío, o el correo introducido no es válido.');
                    return;
                } else if (!$scope.usuario) {
                    utils.alertError('¡El nombre de usuario no puede estar vacío!');
                    return;
                } else if ($scope.usuario.length < 2) {
                    utils.alertError('¡El nombre de usuario debe tener como mínimo 2 caracteres!');
                    return;
                } else if ($scope.usuario.indexOf(" ") > -1) {
                    utils.alertError('¡El nombre de usuario no puede contener espacios!');
                    return;
                } else if (!$scope.userPass && !$scope.realpassword) {
                    utils.alertError('¡El password no puede estar vacío!');
                    return;

                    if ($scope.userPass !== null) {
                        if ($scope.userPass.length < 6) {
                            utils.alertError('¡El password debe tener como mínimo 6 caracteres!');
                            return;
                        }
                        if ($scope.userPass.indexOf(" ") > -1) {
                            utils.alertError('¡El password no puede contener espacios!');
                            return;
                        }
                    }
                }

                var JSONObject;

                if (!$scope.userPass) {

                    JSONObject = {"nombre": $scope.nombre, "apellido": $scope.apellido, "apellido2": $scope.apellido2, "email": localStorage.email, "usuario": localStorage.usuario, "password": localStorage.password, "provincia": $scope.provincia, "canton": $scope.canton, "idUsuario": localStorage.idUsuario, "passwordUser": localStorage.password, "estado": "activo"};

                } else {

                    JSONObject = {"nombre": $scope.nombre, "apellido": $scope.apellido, "apellido2": $scope.apellido2, "email": localStorage.email, "usuario": localStorage.usuario, "password": $scope.userPass, "provincia": $scope.provincia, "canton": $scope.canton, "estado": "activo", "idUsuario": localStorage.idUsuario, "passwordUser": localStorage.password};
                    localStorage.password = $scope.userPass;
                }

                utils.postHttp("gotapUserChange", JSONObject).then(function (resolvedVal) {

                    if (resolvedVal.status === 500) {

                        utils.alertError("No ha sido posible realizar tus modificaciones");

                    } else {
                        document.location = "#/menu/misDatos";


                        utils.alertError("Tus modificaciones han sido realizadas");

                    }
                });

            };

        });
        