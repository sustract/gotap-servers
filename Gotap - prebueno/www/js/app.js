// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'


angular.module('Gotap', ['ionic', 'ngCordova', 'nfcFilters'])
        .config(function ($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('/');

            $stateProvider
                    .state('login', {
                        url: '/',
                        templateUrl: 'templates/login.html',
                        controller: 'LoginController'

                    })
                    .state('registro', {
                        url: '/registro',
                        templateUrl: 'templates/registro.html',
                        controller: 'RegistroController'

                    })
                    .state('validarNumero', {
                        url: '/validarNumero',
                        templateUrl: 'templates/validarNumero.html',
                        controller: 'ValidarNumeroController'

                    })
                    .state('verificarNumero', {
                        url: '/verificarNumero',
                        templateUrl: 'templates/verificarNumero.html',
                        controller: 'ValidarNumeroController'

                    })
                    .state('menu', {
                        url: '/menu',
                        templateUrl: 'templates/menu.html',
                        abstract: true
                    })
                    .state('menu.recargaAhora', {
                        url: '/recargaAhora',
                        templateUrl: 'templates/menu.html',
                        views: {
                            'recargaAhora': {
                                templateUrl: 'templates/recargaAhora.html',
                                controller: 'RecargaAhoraController'
                            }
                        }

                    })
                    .state('menu.misDatos', {
                        url: '/misDatos',
                        templateUrl: 'templates/misDatos.html',
                        views: {
                            'recargaAhora': {
                                templateUrl: 'templates/misDatos.html',
                                controller: 'MisDatosController'
                            }
                        }

                    })
                    .state('menu.mediosDePago', {
                        url: '/mediosDePago',
                        templateUrl: 'templates/mediosDePago.html',
                        views: {
                            'recargaAhora': {
                                templateUrl: 'templates/mediosDePago.html',
                                controller: 'MediosDePagoController'
                            }
                        }

                    })
                    .state('menu.misNumeros', {
                        url: '/misNumeros',
                        templateUrl: 'templates/misNumeros.html',
                        views: {
                            'recargaAhora': {
                                templateUrl: 'templates/misNumeros.html',
                                controller: 'MisNumerosController'
                            }
                        }

                    }) .state('menu.transacciones', {
                        url: '/transacciones',
                        templateUrl: 'templates/transacciones.html',
                        views: {
                            'recargaAhora': {
                                templateUrl: 'templates/transacciones.html',
                                controller: 'TransaccionesController'
                            }
                        }

                    });
        })

       .factory('nfcService', function ($rootScope, $ionicPlatform) {

            var tag = {};

            $ionicPlatform.ready(function () {
                nfc.addNdefListener(function (nfcEvent) {
                    console.log(JSON.stringify(nfcEvent.tag, null, 4));
                    $rootScope.$apply(function () {
                        angular.copy(nfcEvent.tag, tag);
                    });
                }, function () {
                    console.log("Listening for NDEF Tags.");
                }, function (reason) {
                    alert("Error adding NFC Listener " + reason);
                });

            });

            return {
                tag: tag,
                clearTag: function () {
                    angular.copy({}, this.tag);
                }
            };
        });



