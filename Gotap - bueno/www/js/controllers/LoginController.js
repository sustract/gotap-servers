/*
 * Autor @CarloOSX
 */

/* global Function, constructor, angular para poder hacer extends si es necesario */

Function.prototype.extends = function (superclass) {
    var constructor = this.prototype.constructor;
    this.prototype = Object.create(superclass.prototype);
    this.prototype.constructor = constructor;
    return this;
};

/*Definimos el modulo al que pertenecemos*/
angular.module('Gotap')
//My Class
        /*Definimos el controlador que vamos a usar*/
        .controller('LoginController', function (IonicFunctions, /*Starting with Angular injection*/ $scope, $state, $rootScope, $ionicPopup, $timeout, $cordovaVibration, $ionicScrollDelegate, $ionicSlideBoxDelegate) {
            /**
             * Variables globales y objetos
             */

            /*declaramos el objeto ionic con el que podremos acceder a los "métodos" definidas en su clase*/
            utils = new IonicFunctions();
            $scope.utils = utils;

            /* Función de login */

            $scope.prova = function (userName, password) {
                //utils.goToPage('registro');
                console.log(userName, password);
                var JSONObject = {"usuario": userName, "password": password};
                
                   /* $ionicPopup.alert({
                    title: 'Success',
                    content: 'Hello World!!!'
                    }).then(function(res) {
                        console.log('Test Alert Box');
                    });*/
                utils.postHttp("gotapUserLogin", JSONObject).then(function (resolvedVal) {
                    console.log(resolvedVal);
                    debugger;
                    if (resolvedVal.status === 500) {
                        utils.alertError('Usuario o contraseña incorrecto.<br> Por favor revísalos.');
                        return false;
                    } else {
                        //This is called when the promise resolves
                        //console.log(resolvedVal);  logs the value the promise resolves to
                        localStorage.idUsuario = resolvedVal.data.id;
                        localStorage.nombre = resolvedVal.data.nombre;
                        localStorage.apellido = resolvedVal.data.apellido;
                        localStorage.apellido2 = resolvedVal.data.apellido2;
                        localStorage.identificador = resolvedVal.data.identificador;
                        localStorage.email = resolvedVal.data.email;
                        localStorage.usuario = resolvedVal.data.usuario;
                        localStorage.password = resolvedVal.data.password;
                        localStorage.regisTimestamp = resolvedVal.data.fechaRegistro;
                        localStorage.provincia = resolvedVal.data.provincia;
                        localStorage.canton = resolvedVal.data.canton;
                        localStorage.fechaRegistro = utils.formatDate(resolvedVal.data.fechaRegistro);

                        if (resolvedVal.data.estado === "inactivo") {
                            utils.alertError('El usuario ' + resolvedVal.data.usuario.toUpperCase() + ' se dió de baja. <br> Por favor regístrate de nuevo');
                            localStorage.clear();

                            /*   } else if (resolvedVal.data.uuidDevice !== utils.getUUID()) {
                             console.log(resolvedVal.data.uuidDevice + " - " + utils.getUUID());
                             document.location = '#/verificarNumero';*/
                        } else {

                            var JSONObject2 = {"idUsuario": localStorage.idUsuario, "password": localStorage.password};
                            utils.postHttp("gotapGetTelefonos", JSONObject2).then(function (resolvedVal) {
                                
                                if (resolvedVal.status === 500) {
                                    console.log(resolvedVal.status);
                                } else {
                                    console.log(resolvedVal.data.length);
                                    
                                    if (resolvedVal.data.length!==0) {
                                        localStorage.numeroRegistro = resolvedVal.data[0].numero;
                                        //$state.go('menu.recargaAhora');
                                        utils.goToPage('menu/recargaAhora')
                                    } else {
                                        //cambiar
                                        utils.goToPage('menu/recargaAhora')
                                        //utils.goToPage('validarNumero')
                                        //document.location = '#/validarNumero';
                                    }

                                }
                            });
                        }
                    }
                });
            };

            $scope.GoToRegistro = function(){
                document.location = '#/registro';
            };
            
            $scope.goToRecargar = function(){
                $timeout(function(){
                    document.location = '#/bienvenido'
                }, 500); 
            };
        });
