/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('Gotap')

        .factory('IonicFunctions', function ($http, $cordovaDevice, $ionicPopup, $cordovaVibration, $timeout, $state, $ionicModal, $window) {//My Class
            /**
             * Constructor, with class name
             */
            function IonicFunctions() {
                // Public properties, assigned to the instance ('this')
            }

            /*Definimos la direccion http del server*/
            var GoTapApi = "http://appsrv01.sevenandseven.net:28081/GoTapWS/";
            var login = false;
            /**
             * Public method, assigned to prototype
             */

            /*Cogemos el uddid del telefono con el plugin device*/
            /*Se llama automaticamente ondeviceReady*/
            IonicFunctions.prototype.getUUID = function () {
                var uuid = "null";
                /*document.addEventListener("deviceready", function () {
                    uuid = $cordovaDevice.getUUID();
                }, false);*/

                return uuid;
            };

            /* Funcion generica para devolver las respuestas del servidor pasandole unos argumentos */
            IonicFunctions.prototype.postHttp = function (ruta, JSONObject) {

                var jsonData = JSON.stringify(JSONObject);

                return $http.post(GoTapApi + ruta, jsonData).
                        then(function (response) {
                            return response;

                        }, function (response) {
                            return response;
                        });
            };

            /* Funcion generica para devolver las respuestas del servidor pasandole unos argumentos */
            IonicFunctions.prototype.formatDate = function (fecha) {
                if (fecha) {
                    Number.prototype.padLeft = function (base, chr) {
                        var len = (String(base || 10).length - String(this).length) + 1;
                        return len > 0 ? new Array(len).join(chr || '0') + this : this;
                    };
                    var d = new Date(fecha),
                            dformat = [
                                d.getDate().padLeft(),
                                (d.getMonth() + 1).padLeft(),
                                d.getFullYear()].join('/');
                    return dformat;
                }
            };


            /* Funcion generica para pintar alertas de éxito */
            IonicFunctions.prototype.alertSuccess = function (mensaje) {

                var alertPopup = $ionicPopup.alert({
                    title: '¡Enhorabuena!',
                    template: mensaje,
                    buttons: [
                        {
                            text: '<b>Aceptar</b>',
                            type: 'button-gotap',
                            onTap: function () {
                                alertPopup.close();
                            }
                        }
                    ]
                });
                $timeout(function () {
                    alertPopup.close();
                }, 3000);

            };
            /* Funcion generica para pintar alertas de error */
            IonicFunctions.prototype.alertError = function (mensaje) {

                var alertPopup = $ionicPopup.alert({
                    title: 'Atención',
                    template: mensaje,
                    buttons: [
                        {
                            text: '<b>Aceptar</b>',
                            type: 'button-gotap',
                            onTap: function () {
                                alertPopup.close();
                            }
                        }
                    ]
                });
                $cordovaVibration.vibrate(300);
                $timeout(function () {
                    alertPopup.close();
                }, 3000);

            };

            /* Funcion generica para redireccionar entre pantallas, se le pasa la pantalla como argumento */
            IonicFunctions.prototype.goToPage = function (pantalla) {
                document.location = '#/' + pantalla;
            };

            /* Funcion generica para abrir urls externas */
            IonicFunctions.prototype.openWeb = function (link) {
                $window.open(link, '_self', 'location=yes');
            };
            
            /* Funcion generica para obtener información de local storage */
            /*IonicFunctions.prototype.getStorageInfo = function () {
                return localStorage.nombre
            };*/

            /* Funcion generica para loguear des de la modal */
            IonicFunctions.prototype.logoff = function () {
                localStorage.clear();
                IonicFunctions.prototype.goToPage('login');
            };
            
            /* Funcion generica para poner la modal del menú */
            /*IonicFunctions.prototype.showGoTapMenu = function () {

                //$scope.showGotapMenu = {modal:null};

                $ionicModal.fromTemplateUrl('templates/modal-gotapmenu.html', {
                        scope: $scope,
                        animation: 'slide-in-up',
                        focusFirstInput: true
                }).then(function(modal) {
                        $scope.showGotapMenu.modal = modal;
                        $scope.showGotapMenu.openModal();
                });
                $scope.showGotapMenu.openModal = function() {
                        angular.element(document.querySelector('#nav-view-parent')).addClass('blur');
                        $scope.showGotapMenu.modal.show();
                };
                $scope.showGotapMenu.closeModal = function() {
                        $scope.showGotapMenu.modal.hide();
                };
                $scope.$on('modal.hidden', function() {
                        angular.element(document.querySelector('#nav-view-parent')).removeClass('blur');
                });
                $scope.goTo = function (pantalla){
                        $scope.showGotapMenu.closeModal;
                        IonicFunctions.prototype.goToPage('pantalla');
                };
            };*/

            /**
             * Return the constructor function
             */
            return IonicFunctions;
        });
