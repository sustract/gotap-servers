// Docu http://twilio.github.io/twilio-node/#installation

var express = require('express');

var app = express();

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/', function(req, res) { //Get Normal sin parametros
  res.writeHead(200,{"Content-Type": "text/html"});
  res.write("<h1>Servidor SMS de GoTap</h1>");
  res.end();
});

app.get('/SendSMS/:PhoneNumber/:RandomNumber', function(req, res) {
    // EJEMPLO PARA MANDAR REST res.send({id:req.params.id, name: "The Name", description: "description"});
    // Twilio Credentials
    var accountSid = 'AC40f67c6ebceb6b59ce8cc2b2e8c50eb6';
    var authToken = '2a193807ab857f61aaa2594aac7ca607';

    //require the Twilio module and create a REST client
    var client = require('twilio')(accountSid, authToken);

    client.messages.create({
    	to: req.params.PhoneNumber,
    	from: "+13056768777",//Aqui reemplazaremos por la variable del numero que recibiremos en el post
    	body: "Info @CarlosUniqueService: Hora de quedada 11:00am. Localización: Tu casa, amor. El Servicio de SMS de Carlos Martinez le desea una buena noche y que sueñe con el emisor de este SMS."// Reemplazaremos por el random que recibiremos en el post
    }, function(err, message) {
    	console.log(message.sid);
      res.writeHead(200, {'Content-Type': 'text/event-stream'});
    });
      

});

app.listen(28082);
console.log('Puerto para procesar peticiones 28082. Escuchando ...');
