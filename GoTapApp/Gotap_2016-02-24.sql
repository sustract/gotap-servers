# ************************************************************
# Sequel Pro SQL dump
# Versi�n 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.34)
# Base de datos: Gotap
# Tiempo de Generaci�n: 2016-02-23 23:17:32 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla nfcTags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `nfcTags`;

CREATE TABLE `nfcTags` (
  `tag` varchar(100) NOT NULL DEFAULT '',
  `empresa` varchar(100) NOT NULL DEFAULT '',
  `importe` decimal(7,2) NOT NULL,
  `descripcion` varchar(100) NOT NULL DEFAULT '',
  `moneda` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Volcado de tabla tarjetas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tarjetas`;

CREATE TABLE `tarjetas` (
  `id` varchar(255) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `idUsuario` varchar(255) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `numero` bigint(16) NOT NULL,
  `nombrePropietario` varchar(60) NOT NULL DEFAULT '',
  `apellidoPropietario` varchar(60) NOT NULL DEFAULT '',
  `cvc` int(3) NOT NULL,
  `caducidad` date NOT NULL,
  `tipo` varchar(20) NOT NULL DEFAULT '',
  `estado` varchar(8) NOT NULL DEFAULT 'activa',
  PRIMARY KEY (`id`),
  KEY `idUsuario` (`idUsuario`),
  KEY `indiceForeign2` (`numero`),
  CONSTRAINT `tarjetas_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `cambiadoruuid` BEFORE INSERT ON `tarjetas` FOR EACH ROW SET new.id = uuid() */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# Volcado de tabla telefonos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `telefonos`;

CREATE TABLE `telefonos` (
  `id` varchar(255) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `idUsuario` varchar(255) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `numero` varchar(20) NOT NULL DEFAULT '',
  `alias` varchar(20) NOT NULL DEFAULT '',
  `favorito` varchar(20) NOT NULL DEFAULT '',
  `company` varchar(30) NOT NULL DEFAULT '',
  `estado` varchar(8) NOT NULL DEFAULT 'activo',
  PRIMARY KEY (`id`),
  KEY `idUsuario` (`idUsuario`),
  KEY `inidiceForeign` (`numero`),
  CONSTRAINT `telefonos_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `cambiador` BEFORE INSERT ON `telefonos` FOR EACH ROW SET new.id = uuid() */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# Volcado de tabla transacciones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `transacciones`;

CREATE TABLE `transacciones` (
  `id` varchar(255) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `idUsuario` varchar(255) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `numeroTarjeta` bigint(16) NOT NULL,
  `tag` varchar(255) NOT NULL DEFAULT '',
  `importe` decimal(7,2) DEFAULT NULL,
  `telefono` varchar(20) NOT NULL DEFAULT '',
  `fechaTransaccion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `localizacion` varchar(100) NOT NULL DEFAULT '',
  `estado` varchar(15) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `idUsuario` (`idUsuario`),
  KEY `tag` (`tag`),
  KEY `transacciones_ibfk_3` (`telefono`),
  KEY `transacciones_ibfk_4` (`numeroTarjeta`),
  CONSTRAINT `transacciones_ibdfk_tag` FOREIGN KEY (`tag`) REFERENCES `nfcTags` (`tag`),
  CONSTRAINT `transacciones_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`id`),
  CONSTRAINT `transacciones_ibfk_3` FOREIGN KEY (`telefono`) REFERENCES `telefonos` (`numero`),
  CONSTRAINT `transacciones_ibfk_4` FOREIGN KEY (`numeroTarjeta`) REFERENCES `tarjetas` (`numero`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `cambiadoruuids` BEFORE INSERT ON `transacciones` FOR EACH ROW SET new.id = uuid() */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# Volcado de tabla usuarios
# ------------------------------------------------------------

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios` (
  `id` varchar(255) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `nombre` varchar(255) NOT NULL DEFAULT '',
  `apellido` varchar(255) NOT NULL DEFAULT '',
  `apellido2` varchar(255) DEFAULT '',
  `identificador` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `usuario` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `fechaRegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estado` varchar(255) NOT NULL DEFAULT 'activo',
  `uuidDevice` varchar(255) NOT NULL DEFAULT 'Sin uuid',
  `pais` varchar(255) NOT NULL DEFAULT 'Costa Rica',
  `provincia` varchar(255) NOT NULL DEFAULT '',
  `canton` varchar(255) NOT NULL DEFAULT '',
  `direccion` varchar(255) NOT NULL DEFAULT 'Vacío',
  `codigoPostal` varchar(255) NOT NULL DEFAULT 'Vacío',
  PRIMARY KEY (`id`),
  UNIQUE KEY `identificador` (`identificador`),
  UNIQUE KEY `usuario` (`usuario`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `before_insert_usuarios` BEFORE INSERT ON `usuarios` FOR EACH ROW SET new.id = uuid() */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
