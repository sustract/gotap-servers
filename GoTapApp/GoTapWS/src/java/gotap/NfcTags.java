/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gotap;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author CarLoOSX
 */
@Entity
@Table(name = "nfcTags")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NfcTags.findAll", query = "SELECT n FROM NfcTags n"),
    @NamedQuery(name = "NfcTags.findByTag", query = "SELECT n FROM NfcTags n WHERE n.tag = :tag"),
    @NamedQuery(name = "NfcTags.findByEmpresa", query = "SELECT n FROM NfcTags n WHERE n.empresa = :empresa"),
    @NamedQuery(name = "NfcTags.findByImporte", query = "SELECT n FROM NfcTags n WHERE n.importe = :importe"),
    @NamedQuery(name = "NfcTags.findByDescripcion", query = "SELECT n FROM NfcTags n WHERE n.descripcion = :descripcion"),
    @NamedQuery(name = "NfcTags.findByMoneda", query = "SELECT n FROM NfcTags n WHERE n.moneda = :moneda")})
public class NfcTags implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "tag")
    private String tag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "empresa")
    private String empresa;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "importe")
    private BigDecimal importe;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "moneda")
    private String moneda;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tag")
    private Collection<Transacciones> transaccionesCollection;

    public NfcTags() {
    }

    public NfcTags(String tag) {
        this.tag = tag;
    }

    public NfcTags(String tag, String empresa, BigDecimal importe, String descripcion, String moneda) {
        this.tag = tag;
        this.empresa = empresa;
        this.importe = importe;
        this.descripcion = descripcion;
        this.moneda = moneda;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    @XmlTransient
    public Collection<Transacciones> getTransaccionesCollection() {
        return transaccionesCollection;
    }

    public void setTransaccionesCollection(Collection<Transacciones> transaccionesCollection) {
        this.transaccionesCollection = transaccionesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tag != null ? tag.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NfcTags)) {
            return false;
        }
        NfcTags other = (NfcTags) object;
        if ((this.tag == null && other.tag != null) || (this.tag != null && !this.tag.equals(other.tag))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gotap.NfcTags[ tag=" + tag + " ]";
    }
    
}
