/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gotap.service;

import gotap.Transacciones;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author CarLoOSX
 */
@Stateless
@Path("gotap.transacciones")
public class TransaccionesFacadeREST extends AbstractFacade<Transacciones> {
    @PersistenceContext(unitName = "GoTapWSPU")
    private EntityManager em;

    public TransaccionesFacadeREST() {
        super(Transacciones.class);
    }

    @POST
    @Override
    @Consumes({"application/json"})
    public void create(Transacciones entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/json"})
    public void edit(@PathParam("id") Long id, Transacciones entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Long id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/json"})
    public Transacciones find(@PathParam("id") Long id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/json"})
    public List<Transacciones> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/json"})
    public List<Transacciones> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @GET
    @Path("getAllTransactionsById/{idUsuario}")
    @Produces({"application/json"})
    @Override
    public List<Transacciones> getAllTransactionsbyId(@PathParam("idUsuario") Long idUsuario) {
        return super.getAllTransactionsbyId(idUsuario);
    }

    @GET
    @Path("getAllTransactionsByCard/{creditCardNumber}")
    @Produces({"application/json"})
    @Override
    public List<Transacciones> getAllTransactionsbyCard(@PathParam("creditCardNumber") Long creditCardNumber) {
        return super.getAllTransactionsbyCard(creditCardNumber);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
