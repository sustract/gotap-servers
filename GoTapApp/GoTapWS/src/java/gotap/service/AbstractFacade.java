/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gotap.service;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author CarLoOSX
 */
public abstract class AbstractFacade<T> {
    private Class<T> entityClass;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected abstract EntityManager getEntityManager();

    public void create(T entity) {
        getEntityManager().persist(entity);
    }

    public void edit(T entity) {
        getEntityManager().merge(entity);
    }

    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
    List<T> getAllCreditCards(Long idUsuario) {
        String sql = "SELECT * FROM tarjetas WHERE idUsuario=" + idUsuario;
        Query query = getEntityManager().createNativeQuery(sql, entityClass);
        return query.getResultList();
    }

    List<T> getAllTransactionsbyId(Long idUsuario) {
        String sql = "SELECT * FROM transacciones WHERE idUsuario=" + idUsuario;
        Query query = getEntityManager().createNativeQuery(sql, entityClass);
        return query.getResultList();
    }

    List<T> getAllTransactionsbyCard(Long creditCard) {
        String sql = "SELECT * FROM transacciones WHERE numeroTarjeta=" + creditCard;
        Query query = getEntityManager().createNativeQuery(sql, entityClass);
        return query.getResultList();
    }

    /*List<T> getAllDirections(Long idUsuario) {
        String sql = "SELECT * FROM direcciones WHERE idUsuario=" + idUsuario;
        Query query = getEntityManager().createNativeQuery(sql, entityClass);
        return query.getResultList();
    }*/

    List<T> getAllPhoneNumbers(Long idUsuario) {
        String sql = "SELECT * FROM telefonos WHERE idUsuario=" + idUsuario;
        Query query = getEntityManager().createNativeQuery(sql, entityClass);
        return query.getResultList();
    }

    public T doLogin(String user, String password) {

        String sql = "SELECT * FROM usuarios WHERE usuario= '" + user + "' AND password='" + password + "';";
        Query query = getEntityManager().createNativeQuery(sql, entityClass);

        return (T) query.getSingleResult();

    }

    public T buscarIdentificador(String identificador) {
        String sql = "SELECT * FROM usuarios WHERE identificador= '" + identificador + "';";
        Query query = getEntityManager().createNativeQuery(sql, entityClass);

        return (T) query.getSingleResult();
    }

    public T buscarUsuario(String usuario) {
        String sql = "SELECT * FROM usuarios WHERE usuario= '" + usuario + "';";
        Query query = getEntityManager().createNativeQuery(sql, entityClass);

        return (T) query.getSingleResult();
    }

    public T buscarEmail(String email) {
        String sql = "SELECT * FROM usuarios WHERE email= '" + email + "';";
        Query query = getEntityManager().createNativeQuery(sql, entityClass);

        return (T) query.getSingleResult();
    }


    
}
