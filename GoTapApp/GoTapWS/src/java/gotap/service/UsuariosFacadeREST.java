/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gotap.service;

import gotap.Usuarios;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author CarLoOSX
 */
@Stateless
@Path("gotap.usuarios")
public class UsuariosFacadeREST extends AbstractFacade<Usuarios> {
    @PersistenceContext(unitName = "GoTapWSPU")
    private EntityManager em;

    public UsuariosFacadeREST() {
        super(Usuarios.class);
    }

    @POST
    @Override
    @Consumes({"application/json"})
    public void create(Usuarios entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/json"})
    public void edit(@PathParam("id") Long id, Usuarios entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Long id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/json"})
    public Usuarios find(@PathParam("id") Long id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/json"})
    public List<Usuarios> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/json"})
    public List<Usuarios> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @GET
    @Path("doLogin/{user}/{password}")
    @Consumes("text/plain")
    @Produces({"application/json"})
    @Override
    public Usuarios doLogin(@PathParam("user") String user, @PathParam("password") String password) {
        System.out.println("PASO PORA AKI");
        return super.doLogin(user, password);
    }

    @GET
    @Path("buscarUsuario/{usuario}")
    @Consumes("text/plain")
    @Produces({"application/json"})
    @Override
    public Usuarios buscarUsuario(@PathParam("usuario") String usuario) {

        return super.buscarUsuario(usuario);
    }

    @GET
    @Path("buscarIdentificador/{identificador}")
    @Consumes("text/plain")
    @Produces({"application/json"})
    @Override
    public Usuarios buscarIdentificador(@PathParam("identificador") String identificador) {

        return super.buscarIdentificador(identificador);
    }

    @GET
    @Path("buscarEmail/{email}")
    @Consumes("text/plain")
    @Produces({"application/json"})
    @Override
    public Usuarios buscarEmail(@PathParam("email") String email) {

        return super.buscarEmail(email);
    }


    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
