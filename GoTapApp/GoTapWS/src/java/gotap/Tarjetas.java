/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gotap;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author CarLoOSX
 */
@Entity
@Table(name = "tarjetas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tarjetas.findAll", query = "SELECT t FROM Tarjetas t"),
    @NamedQuery(name = "Tarjetas.findById", query = "SELECT t FROM Tarjetas t WHERE t.id = :id"),
    @NamedQuery(name = "Tarjetas.findByNumero", query = "SELECT t FROM Tarjetas t WHERE t.numero = :numero"),
    @NamedQuery(name = "Tarjetas.findByPropietario", query = "SELECT t FROM Tarjetas t WHERE t.propietario = :propietario"),
    @NamedQuery(name = "Tarjetas.findByCvc", query = "SELECT t FROM Tarjetas t WHERE t.cvc = :cvc"),
    @NamedQuery(name = "Tarjetas.findByCaducidad", query = "SELECT t FROM Tarjetas t WHERE t.caducidad = :caducidad"),
    @NamedQuery(name = "Tarjetas.findByTipo", query = "SELECT t FROM Tarjetas t WHERE t.tipo = :tipo"),
    @NamedQuery(name = "Tarjetas.findByEstado", query = "SELECT t FROM Tarjetas t WHERE t.estado = :estado")})
public class Tarjetas implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "numero")
    private long numero;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "propietario")
    private String propietario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cvc")
    private int cvc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "caducidad")
    @Temporal(TemporalType.DATE)
    private Date caducidad;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "tipo")
    private String tipo;
    @Size(max = 8)
    @Column(name = "estado")
    private String estado;
    @JoinColumn(name = "idUsuario", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Usuarios idUsuario;

    public Tarjetas() {
    }

    public Tarjetas(Long id) {
        this.id = id;
    }

    public Tarjetas(Long id, long numero, String propietario, int cvc, Date caducidad, String tipo) {
        this.id = id;
        this.numero = numero;
        this.propietario = propietario;
        this.cvc = cvc;
        this.caducidad = caducidad;
        this.tipo = tipo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getNumero() {
        return numero;
    }

    public void setNumero(long numero) {
        this.numero = numero;
    }

    public String getPropietario() {
        return propietario;
    }

    public void setPropietario(String propietario) {
        this.propietario = propietario;
    }

    public int getCvc() {
        return cvc;
    }

    public void setCvc(int cvc) {
        this.cvc = cvc;
    }

    public Date getCaducidad() {
        return caducidad;
    }

    public void setCaducidad(Date caducidad) {
        this.caducidad = caducidad;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Usuarios getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuarios idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tarjetas)) {
            return false;
        }
        Tarjetas other = (Tarjetas) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gotap.Tarjetas[ id=" + id + " ]";
    }
    
}
