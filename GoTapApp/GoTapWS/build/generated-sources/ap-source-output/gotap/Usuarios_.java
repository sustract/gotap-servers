package gotap;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Usuarios.class)
public abstract class Usuarios_ {

	public static volatile SingularAttribute<Usuarios, String> apellido2;
	public static volatile SingularAttribute<Usuarios, String> estado;
	public static volatile SingularAttribute<Usuarios, String> uuidDevice;
	public static volatile SingularAttribute<Usuarios, String> codigoPostal;
	public static volatile CollectionAttribute<Usuarios, Transacciones> transaccionesCollection;
	public static volatile SingularAttribute<Usuarios, Date> fechaRegistro;
	public static volatile SingularAttribute<Usuarios, String> direccion;
	public static volatile SingularAttribute<Usuarios, String> nombre;
	public static volatile SingularAttribute<Usuarios, String> pais;
	public static volatile SingularAttribute<Usuarios, String> password;
	public static volatile SingularAttribute<Usuarios, String> cantonCiudad;
	public static volatile SingularAttribute<Usuarios, String> apellido;
	public static volatile SingularAttribute<Usuarios, String> usuario;
	public static volatile SingularAttribute<Usuarios, Long> id;
	public static volatile SingularAttribute<Usuarios, String> provinciaEstado;
	public static volatile CollectionAttribute<Usuarios, Telefonos> telefonosCollection;
	public static volatile CollectionAttribute<Usuarios, Tarjetas> tarjetasCollection;
	public static volatile SingularAttribute<Usuarios, String> email;
	public static volatile SingularAttribute<Usuarios, String> identificador;

}

