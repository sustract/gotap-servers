package gotap;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Telefonos.class)
public abstract class Telefonos_ {

	public static volatile SingularAttribute<Telefonos, String> numero;
	public static volatile SingularAttribute<Telefonos, String> favorito;
	public static volatile SingularAttribute<Telefonos, Usuarios> idUsuario;
	public static volatile SingularAttribute<Telefonos, String> alias;
	public static volatile SingularAttribute<Telefonos, String> company;
	public static volatile SingularAttribute<Telefonos, Long> id;
	public static volatile SingularAttribute<Telefonos, String> activo;

}

