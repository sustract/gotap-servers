package gotap;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Tarjetas.class)
public abstract class Tarjetas_ {

	public static volatile SingularAttribute<Tarjetas, Integer> cvc;
	public static volatile SingularAttribute<Tarjetas, Date> caducidad;
	public static volatile SingularAttribute<Tarjetas, String> tipo;
	public static volatile SingularAttribute<Tarjetas, String> estado;
	public static volatile SingularAttribute<Tarjetas, Long> numero;
	public static volatile SingularAttribute<Tarjetas, String> propietario;
	public static volatile SingularAttribute<Tarjetas, Usuarios> idUsuario;
	public static volatile SingularAttribute<Tarjetas, Long> id;

}

