package gotap;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Transacciones.class)
public abstract class Transacciones_ {

	public static volatile SingularAttribute<Transacciones, String> estado;
	public static volatile SingularAttribute<Transacciones, Date> fechaTransaccion;
	public static volatile SingularAttribute<Transacciones, Usuarios> idUsuario;
	public static volatile SingularAttribute<Transacciones, String> localizacion;
	public static volatile SingularAttribute<Transacciones, Long> id;
	public static volatile SingularAttribute<Transacciones, NfcTags> tag;
	public static volatile SingularAttribute<Transacciones, Long> numeroTarjeta;

}

