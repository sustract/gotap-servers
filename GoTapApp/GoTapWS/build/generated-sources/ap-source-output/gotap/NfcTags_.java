package gotap;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(NfcTags.class)
public abstract class NfcTags_ {

	public static volatile SingularAttribute<NfcTags, String> descripcion;
	public static volatile CollectionAttribute<NfcTags, Transacciones> transaccionesCollection;
	public static volatile SingularAttribute<NfcTags, String> moneda;
	public static volatile SingularAttribute<NfcTags, String> tag;
	public static volatile SingularAttribute<NfcTags, String> empresa;
	public static volatile SingularAttribute<NfcTags, BigDecimal> importe;

}

