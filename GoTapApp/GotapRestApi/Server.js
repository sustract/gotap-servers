
//**************************************************************************************************************************************************************************************************************

//************************************************************************************ DEFINIMOS LO NECESARIO *******************************************************************************************

//**************************************************************************************************************************************************************************************************************
var express = require('express'),
        app = express(),
        mysql = require('mysql'),
        connectionpool = mysql.createPool({
            host: 'localhost',
            user: 'root',
            password: 'IwvnUh%Lg0b54nes%',
            database: 'Gotap'
        });
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded());
app.use(bodyParser.json());
var parser = require('xml2json');

//**************************************************************************************************************************************************************************************************************

//************************************************************************************ COMENZAMOS LOS METODOS DEL WS *******************************************************************************************

//**************************************************************************************************************************************************************************************************************


//************************************************************************************ USUARIOS CREATE UPDATE Y LOGIN *******************************************************************************************


app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/GoTapWS', function (req, res) { //Get Normal sin parametros para ver si el server esta activo ¿ Lo ocultamos por seguridad ?
    res.writeHead(200, {"Content-Type": "text/html"});
    res.write("<h1>Servidor Restful de GoTap</h1>");
    res.end();
});




app.post('/GoTapWS/gotapUserLogin', function (req, res) {
    connectionpool.getConnection(function (err, connection) {
        if (err) {  
        } else { 
            connection.query("SELECT * FROM usuarios WHERE usuario = '" + req.body.usuario + "' AND password = '" + req.body.password + "'; ", function (err, rows, fields) {
                if (err) {
                    res.sendStatus(res.statusCode);
                } else if (rows.length === 0) {
                    res.statusCode = 500;
                    res.send("Error - incorrect user or password, try again");
                } else {    
                    res.send(rows[0]); //podria poner todos los headers que quisiera igual que en todos los sends , hasta podria definir el numero de error y lo que significa
                  
                }
            });
        }
        connection.release();
    });
});




app.post('/GoTapWS/gotapUserRegistration', function (req, res) {
    connectionpool.getConnection(function (err, connection) {
        if (err) {
           
        } else {
            connection.query("INSERT INTO usuarios (nombre,apellido,apellido2,identificador,email,usuario,password,uuidDevice,provincia,canton,direccion,codigoPostal) VALUES ('" + req.body.nombre + "','" + req.body.apellido + "','" + req.body.apellido2 + "','" + req.body.identificador + "','" + req.body.email + "','" + req.body.usuario + "','" + req.body.password + "','" + req.body.uuidDevice + "','" + req.body.provincia + "','" + req.body.canton + "','" + req.body.direccion + "','" + req.body.codigoPostal + "')", function (err, rows, fields) {
                if (err) {
                    res.statusCode = 500;
                    if (err.stack.indexOf("Duplicate entry") > -1) {
                        if (err.stack.indexOf("identificador") > -1) {
                            res.send("El identificador ya está en uso");
                        } else if (err.stack.indexOf("email") > -1) {
                            res.send("El e-mail ya está en uso");
                        } else if (err.stack.indexOf("usuario") > -1) {
                            res.send("El nombre de usuario ya está en uso");
                        }
                    } else { //Excede el tamaño de las columnas  res.send(err.stack);
                        res.sendStatus(res.statusCode);
                    }

                } else {
                    res.sendStatus(res.statusCode);
                }
            });
        }
        connection.release();
    });
});




app.post('/GoTapWS/gotapUserChange', function (req, res) {
    connectionpool.getConnection(function (err, connection) {
        if (err) {
          
        } else {
            connection.query("UPDATE usuarios SET nombre='" + req.body.nombre + "', apellido='" + req.body.apellido + "', apellido2='" + req.body.apellido2 + "',email='" + req.body.email + "',usuario='" + req.body.usuario + "',password='" + req.body.password + "', provincia='" + req.body.provincia + "',canton='" + req.body.canton + "', direccion='" + req.body.direccion + "',codigoPostal='" + req.body.codigoPostal + "',estado='" + req.body.estado + "' WHERE id='" + req.body.idUsuario + "' AND password='" + req.body.passwordUser + "'", function (err, rows, fields) {
                if (err) {
                    res.statusCode = 500;
                    if (err.stack.indexOf("Duplicate entry") > -1) {
                        if (err.stack.indexOf("identificador") > -1) {
                            res.send("El identificador ya está en uso");
                        } else if (err.stack.indexOf("email") > -1) {
                            res.send("El e-mail ya está en uso");
                        } else if (err.stack.indexOf("usuario") > -1) {
                            res.send("El nombre de usuario ya está en uso");
                        }
                    }
                } else if (rows.affectedRows === 0) {
                    res.statusCode = 500;
                    res.send("Usuario i/o contraseña incorrectos");
                } else {
                    res.sendStatus(res.statusCode);
                }
            });
        }
        connection.release();
    });
});




//************************************************************************************ TELEFONOS CREATE UPDATE *******************************************************************************************




app.post('/GoTapWS/gotapInsertTelefono', function (req, res) {
    connectionpool.getConnection(function (err, connection) {
        if (err) { 
        } else {
            connection.query("SELECT password FROM usuarios WHERE id = '" + req.body.idUsuario + "' AND password = '" + req.body.password + "'; ", function (err, rows, fields) {
                if (err) {
                    res.sendStatus(res.statusCode);
                } else if (rows.length === 0) {
                    res.statusCode = 500;
                    res.send("Usuario i/o contraseña incorrectos");
                } else {
                    connection.query("INSERT INTO telefonos (idUsuario,numero,alias,favorito,company) VALUES ('" + req.body.idUsuario + "','" + req.body.numero + "','" + req.body.alias + "','" + req.body.favorito + "','" + req.body.company + "')", function (err, rows, fields) {
                        if (err) {
                            res.send("No se ha podido añadir telefono");
                        } else {
                            res.sendStatus(res.statusCode);
                        }
                    });
                }
            });
        }
        connection.release();
    });
});




app.post('/GoTapWS/gotapUpdateTelefono', function (req, res) {
    connectionpool.getConnection(function (err, connection) {
        if (err) {  
        } else {
            connection.query("SELECT password FROM usuarios WHERE id = '" + req.body.idUsuario + "' AND password = '" + req.body.password + "'; ", function (err, rows, fields) {
                if (err) {
                    res.sendStatus(res.statusCode);
                } else if (rows.length === 0) {
                    res.statusCode = 500;
                    res.send("Usuario i/o contraseña incorrectos");
                } else {
                    connection.query("UPDATE telefonos SET numero='" + req.body.numero + "', alias='" + req.body.alias + "', favorito='" + req.body.favorito + "',company='" + req.body.company + "', estado='" + req.body.estado + "' WHERE idUsuario='" + req.body.idUsuario + "' AND id='" + req.body.id + "'", function (err, rows, fields) {
                        if (err) {
                            res.statusCode = 500;
                            res.send("No se han podido modificar los datos");
                        } else {
                            res.sendStatus(res.statusCode);
                        }
                    });
                }
            });
        }
        connection.release();
    });
});




app.post('/GoTapWS/gotapGetTelefonos', function (req, res) {
    connectionpool.getConnection(function (err, connection) {
        if (err) {
        } else {  
            connection.query("SELECT * FROM usuarios WHERE id = '" + req.body.idUsuario + "' AND password = '" + req.body.password + "'; ", function (err, rows, fields) {
                if (err) {
                    res.sendStatus(res.statusCode);
                } else if (rows.length === 0) {
                    res.statusCode = 500;
                    res.send("Usuario i/o contraseña incorrectos");
                } else {   
                    connection.query("SELECT * FROM telefonos WHERE idUsuario='" + req.body.idUsuario + "'", function (err, rows, fields) {
                        if (err) {
                            res.statusCode = 500;
                            res.send("No se han podido traer los telefonos");
                        } else {
                            res.send(rows);
                        }
                    });
                }
            });
        }
        connection.release();
    });
});




//************************************************************************************ TARJETAS CREATE UPDATE *******************************************************************************************




app.post('/GoTapWS/gotapInsertTarjeta', function (req, res) {
    connectionpool.getConnection(function (err, connection) {
        if (err) {  
        } else {
            connection.query("SELECT password FROM usuarios WHERE id = '" + req.body.idUsuario + "' AND password = '" + req.body.password + "'; ", function (err, rows, fields) {
                if (err) {
                    res.sendStatus(res.statusCode);
                } else if (rows.length === 0) {
                    res.statusCode = 500;
                    res.send("Usuario i/o contraseña incorrectos");
                } else {
                    connection.query("INSERT INTO tarjetas (idUsuario,numero,nombrePropietario,apellidoPropietario,cvc,caducidad,tipo) VALUES ('" + req.body.idUsuario + "','" + req.body.numero + "','" + req.body.nombrePropietario + "','" + req.body.apellidoPropietario + "','" + req.body.cvc + "','" + req.body.caducidad + "','" + req.body.tipo + "')", function (err, rows, fields) {
                        if (err) {
                            res.send("La tarjeta no ha podido ser añadida");
                        } else {
                            res.sendStatus(res.statusCode);
                        }
                    });
                }
            });
        }
        connection.release();
    });
});




app.post('/GoTapWS/gotapUpdateTarjeta', function (req, res) {
    connectionpool.getConnection(function (err, connection) {
        if (err) { 
        } else {
            connection.query("SELECT password FROM usuarios WHERE id = '" + req.body.idUsuario + "' AND password = '" + req.body.password + "'; ", function (err, rows, fields) {
                if (err) {
                    res.sendStatus(res.statusCode);
                } else if (rows.length === 0) {
                    res.statusCode = 500;
                    res.send("Usuario i/o contraseña incorrectos");
                } else {
                    connection.query("UPDATE tarjetas SET estado='" + req.body.estado + "' WHERE idUsuario='" + req.body.idUsuario + "' AND id='" + req.body.id + "'", function (err, rows, fields) {
                        if (err) {
                            res.statusCode = 500;
                            res.send("La tarjeta no ha podido ser modificada");
                        } else {
                            res.sendStatus(res.statusCode);
                        }
                    });
                }
            });
        }
        connection.release();
    });
});




app.post('/GoTapWS/gotapGetTarjetas', function (req, res) {
    connectionpool.getConnection(function (err, connection) {
        if (err) {  
        } else { 
            connection.query("SELECT * FROM usuarios WHERE id = '" + req.body.idUsuario + "' AND password = '" + req.body.password + "'; ", function (err, rows, fields) {
                if (err) {
                    res.sendStatus(res.statusCode);
                } else if (rows.length === 0) {
                    res.statusCode = 500;
                    res.send("Usuario i/o contraseña incorrectos");
                } else {    
                    connection.query("SELECT * FROM tarjetas WHERE idUsuario='" + req.body.idUsuario + "'", function (err, rows, fields) {
                        if (err) {
                            res.statusCode = 500;
                            res.send("No se han podido importar las tarjetas");
                        } else {
                            res.send(rows);
                        }
                    });
                }
            });
        }
        connection.release();
    });
});


app.post('/GoTapWS/gotapGetTransactions', function (req, res) {
    connectionpool.getConnection(function (err, connection) {
        if (err) {  
        } else { 
            connection.query("SELECT * FROM usuarios WHERE id = '" + req.body.idUsuario + "' AND password = '" + req.body.password + "'; ", function (err, rows, fields) {
                if (err) {
                    res.sendStatus(res.statusCode);
                } else if (rows.length === 0) {
                    res.statusCode = 500;
                    res.send("Usuario i/o contraseña incorrectos");
                } else {    
                    connection.query("SELECT * FROM transacciones WHERE idUsuario='" + req.body.idUsuario + "'", function (err, rows, fields) {
                        if (err) {
                            res.statusCode = 500;
                            res.send("No se han podido importar las tarjetas");
                        } else {
                            res.send(rows);
                        }
                    });
                }
            });
        }
        connection.release();
    });
});


app.post('/GoTapWS/gotapAddTransaction', function (req, res) {
    
    var importe;
    var estado;
    connectionpool.getConnection(function (err, connection) {
        if (err) { 
        } else {
            connection.query("SELECT password FROM usuarios WHERE id = '" + req.body.idUsuario + "' AND password = '" + req.body.password + "'; ", function (err, rows, fields) {
                if (err) {
                    res.sendStatus(res.statusCode);
                } else if (rows.length === 0) {
                    res.statusCode = 500;
                    res.send("Usuario i/o contraseña incorrectos");
                } else {
                    connection.query("SELECT importe from nfcTags WHERE tag = '" + req.body.tag + "'", function (err, rows, fields) {
                        if (err) {
                            res.send("No se reconoce el tag");
                            return;
                        }
                        importe = rows[0].importe;
                        var request = require('request');
                        // Establecemos los headers
                        var headers = {
                            'User-Agent': 'Super Agent/0.0.1',
                            'Content-Type': 'application/x-www-form-urlencoded'
                        };
                        // Configuramos la peticion
                        var options = {
                            url: 'http://gwt2.sevenandseven.net/ws/recarga.php',
                            method: 'POST',
                            headers: headers,
                            form: {"storeID": req.body.storeID, "storePwd": req.body.storePwd, "holderName": req.body.holderName, "holderLastName": req.body.holderLastName, "holderID": req.body.holderID, "NFCID": req.body.NFCID, "amount": importe, "holderPhone": req.body.holderPhone, "ccnumber": req.body.ccnumber, "expmonth": req.body.expmonth, "expyear": req.body.expyear, "cvv": req.body.cvv, "orderid": req.body.orderid}
                        };
                        
                        console.log({"storeID": req.body.storeID, "storePwd": req.body.storePwd, "holderName": req.body.holderName, "holderLastName": req.body.holderLastName, "holderID": req.body.holderID, "NFCID": req.body.NFCID, "amount": importe, "holderPhone": req.body.holderPhone, "ccnumber": req.body.ccnumber, "expmonth": req.body.expmonth, "expyear": req.body.expyear, "cvv": req.body.cvv, "orderid": req.body.orderid});
                        // Lanzamos la peticion
                        request(options, function (error, response, body) {
                            if (!error && response.statusCode === 200) {
                                var options = {// hay mas opciones pero esta es super necesaria para poder acceder como si fuera un objeto osea con .
                                    object: true
                                };
                                var json = parser.toJson(body, options);
                                if (json.Request.Transaction.Recarga.status === "NO PROCESADA") {
                                    estado = "No procesada";

                                } else {
                                    estado = "Realizada";
                                }
                       
                                connection.query("INSERT INTO transacciones (idUsuario,numeroTarjeta,tag,importe,telefono,localizacion,estado) VALUES ('" + req.body.idUsuario + "','" + req.body.ccnumber + "','" + req.body.tag + "'," + importe + ",'" + req.body.holderPhone + "','" + req.body.localizacion + "','" + estado + "')", function (err, rows, fields) {
                                    if (err) {
                               
                                        res.send("La transaccion no ha podido ser añadida");
                                    } else {
                                        res.send(json);
                           
                                    }
                                });
                            } else {

                                console.log("PASARELA DE PAGO NO OPERATIVA");

                            }
                        });
                    });
                }
            });
        }
        connection.release();
    });
});




app.listen(28081);
console.log('Restful Gotap Server escuchando en el puerto: 28081');
