
var GoTapApi = "http://appsrv01.sevenandseven.net:28081/GoTapWS/";

                function formatDate(date) {
                    var d = new Date(date),
                            month = '' + (d.getMonth() + 1),
                            day = '' + d.getDate(),
                            year = d.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                }


function eliminarNumero(numeroButton) {
    JSONObject = {"id": localStorage['telefonoId' + numeroButton].toString(), "numero": localStorage['telefonoNumero' + numeroButton].toString(), "alias": localStorage['telefonoAlias' + numeroButton], "favorito": localStorage['telefonoFavorito' + numeroButton], "activo": "inactivo", "idUsuario": {"id": parseInt(localStorage.idUsuario), "nombre": localStorage.nombre, "apellido": localStorage.apellido, "apellido2": localStorage.apellido2, "identificador": localStorage.identificador, "email": localStorage.email, "usuario": localStorage.usuario, "password": localStorage.password, "fechaRegistro": localStorage.regisTimestamp, "estado": "activo"}};

    var jsonData = JSON.stringify(JSONObject);
    $.ajax({
        type: 'POST',
        url: GoTapApi + "gotap.telefonos/" + parseInt(localStorage.idUsuario),
        data: jsonData,
        timeout: 5000,
        headers: {
            'Content-Type': 'application/json'
        },
        success: function () {
            location.reload();
        },
        error: function () {
            alert("todoMal");
        }

    });

}

function eliminarTarjeta(numeroButton) {
    JSONObject = {"id": localStorage['tarjetaId' + numeroButton].toString(), "numero": parseInt(localStorage['tarjetaNumero' + numeroButton]), "nombrePropietario": localStorage['tarjetaPropietario' + numeroButton],"apellidoPropietario":"", "cvc": parseInt(localStorage['tarjetaCvc' + numeroButton]), "caducidad": formatDate(localStorage['tarjetaFecha' + numeroButton]), "tipo": localStorage['tarjetaTipo' + numeroButton], "estado": "inactiva", "idUsuario": localStorage.idUsuario, "password":localStorage.password};
console.log(JSONObject);
    var jsonData = JSON.stringify(JSONObject);
    $.ajax({
        type: 'POST',
        url: GoTapApi + "gotapUpdateTarjeta",
        data: jsonData,
        timeout: 5000,
        headers: {
            'Content-Type': 'application/json'
        },
        success: function (response) {
            location.reload();
        },
        error: function (response) {
           console.log(response);
        }

    });

}


angular.module('nfcPay.controllers', ['ionic', 'ngCordova'])
        // ('nombreDelModulo',[dependencias])--> este es nuestro fichero de controladores
        /*---------------------------------------------CONTROLADOR DE INDEX---------------------------------------------*/
        //Definicion de variables (finales) provincias y la ruta al WS

        .controller('appFunctions', function ($scope, $http, $ionicPopup, $timeout, $cordovaVibration, $ionicScrollDelegate, $ionicSlideBoxDelegate, $cordovaDevice) {

            //Cogemos el uddid del telefono con el plugin device
            var uuid = "null";
            document.addEventListener("deviceready", function () {
                uuid = $cordovaDevice.getUUID();
            }, false);
            //Recogemos el uuid del telefono //

            $scope.goBack = function () {
                document.location = "Login.html";
            };

            $(function () {
                $('#fitin div').css('font-size', '1em');

                while ($('#fitin div').height() > $('#fitin').height()) {
                    $('#fitin div').css('font-size', (parseInt($('#fitin div').css('font-size')) - 1) + "px");
                }

            });
            $scope.provincias = [{provincia: "San José"}, {provincia: "Alajuela"}, {provincia: "Cartago"}, {provincia: "Heredia"}, {provincia: "Limón"}, {provincia: "Guanacaste"}, {provincia: "Puntarenas"}];

            /*---------------------------------------------Funciones para el Registro y Login---------------------------------------------*/

            //obtenemos el array de cantones en funcion de la provincia
            $scope.getCantones = function (provincia) {

                switch (provincia) {
                    case "San José":
                        $scope.cantones = [{canton: "San José"}, {canton: "Escazú"}, {canton: "Desamparados"}, {canton: "Puriscal"}, {canton: "Tarrazú"}, {canton: "Aserrí"}, {canton: "Mora"}, {canton: "Goicoechea"}, {canton: "Santa Ana"}, {canton: "Alajuelita"}, {canton: "Vásquez de Coronado"}, {canton: "Acosta"}, {canton: "Tibás"}, {canton: "Moravia"}, {canton: "Montes de Oca"}, {canton: "Turrubares"}, {canton: "Dota"}, {canton: "Curridabat"}, {canton: "Pérez Zeledón"}, {canton: "León Cortés Castro"}];
                        break;
                    case "Alajuela":
                        $scope.cantones = [{canton: "Alajuela"}, {canton: "San Ramón"}, {canton: "Grecia"}, {canton: "San Mateo"}, {canton: "Atenas"}, {canton: "Naranjo"}, {canton: "Palmares"}, {canton: "Poás"}, {canton: "Orotina"}, {canton: "San Carlos"}, {canton: "Zarcero"}, {canton: "Valverde Vega"}, {canton: "Upala"}, {canton: "Los Chiles"}, {canton: "Guatuso"}];
                        break;
                    case "Cartago":
                        $scope.cantones = [{canton: "Cartago"}, {canton: "Paraíso"}, {canton: "La unión"}, {canton: "Jimenez"}, {canton: "Turriabla"}, {canton: "Alvarado"}, {canton: "Oreamuno"}, {canton: "El Guarco"}];
                        break;
                    case "Heredia":
                        $scope.cantones = [{canton: "Heredia"}, {canton: "Barva"}, {canton: "Santo Domingo"}, {canton: "Santa Bárbara"}, {canton: "San Rafael"}, {canton: "San Isidro"}, {canton: "Belén"}, {canton: "Flores"}, {canton: "San Pablo"}, {canton: "Sarapiquí"}];
                        break;
                    case "Limón":
                        $scope.cantones = [{canton: "Limón"}, {canton: "Pococí"}, {canton: "Siquirres"}, {canton: "Talamanca"}, {canton: "Matina"}, {canton: "Guácimo"}];
                    case "Guanacaste":
                        $scope.cantones = [{canton: "Liberia"}, {canton: "Nicoya"}, {canton: "Santa Cruz"}, {canton: "Bagaces"}, {canton: "Carrillo"}, {canton: "Cañas"}, {canton: "Abangares"}, {canton: "Tilarán"}, {canton: "Nandayure"}, {canton: "La Cruz"}, {canton: "Hojancha"}];
                    case "Puntarenas":
                        $scope.cantones = [{canton: "Puntarenas"}, {canton: "Esparza"}, {canton: "Buenos Aires"}, {canton: "Montes de Oro"}, {canton: "Osa"}, {canton: "Quepos"}, {canton: "Golfito"}, {canton: "Coto Brus"}, {canton: "Parrita"}, {canton: "Corredores"}, {canton: "Garabito"}];
                        break;
                    default:
                }
            };


            $scope.userLogIn = function (userName, password) {

                //Eliminamos el espacio de las tarjetas numero.replace(/ /g
                var JSONObject = {"usuario": userName.toString(), "password": password.toString()};

                var jsonData = JSON.stringify(JSONObject);
                $http.post(GoTapApi + "gotapUserLogin", jsonData).
                        then(function (response) {

                            if (response.data.estado === "inactivo") {

                                $scope.alertSimpleError('¡El usuario ' + response.data.usuario + ' está dado de baja.\n Por favor regístrate de nuevo!');

                            }

                            if (response.data.uuidDevice !== uuid) {
                                localStorage.idUsuario = response.data.id;
                                localStorage.nombre = response.data.nombre;
                                localStorage.apellido = response.data.apellido;
                                localStorage.apellido2 = response.data.apellido2;
                                localStorage.identificador = response.data.identificador;
                                localStorage.email = response.data.email;
                                localStorage.usuario = response.data.usuario;
                                localStorage.password = response.data.password;
                                localStorage.regisTimestamp = response.data.fechaRegistro;
                                localStorage.provinciaEstado = response.data.provinciaEstado;
                                localStorage.cantonCiudad = response.data.cantonCiudad;
                                document.location = "VerificarNumero.html";
                            } else {

                                localStorage.idUsuario = response.data.id;
                                localStorage.nombre = response.data.nombre;
                                localStorage.apellido = response.data.apellido;
                                localStorage.apellido2 = response.data.apellido2;
                                localStorage.identificador = response.data.identificador;
                                localStorage.email = response.data.email;
                                localStorage.usuario = response.data.usuario;
                                localStorage.password = response.data.password;
                                localStorage.regisTimestamp = response.data.fechaRegistro;
                                localStorage.provinciaEstado = response.data.provinciaEstado;
                                localStorage.cantonCiudad = response.data.cantonCiudad;
                                var fecha = $scope.formatDate(response.data.fechaRegistro);
                                localStorage.fechaRegistro = fecha;

                                if ($scope.isRegistro) {
                                    document.location = 'ValidarNumero.html';
                                } else {
                                    document.location = 'RecargaAhora.html';
                                }
                            }

                        }, function (response) {
                            $scope.alertSimpleError('Usuario i/o contraseña incorrectos');
                            return;
                        });
            };
            //Convertimos el timestamp que nos devuelve WildFly a algo entendible
            $scope.formatDate = function (value) {
                if (value) {
                    Number.prototype.padLeft = function (base, chr) {
                        var len = (String(base || 10).length - String(this).length) + 1;
                        return len > 0 ? new Array(len).join(chr || '0') + this : this;
                    };
                    var d = new Date(value),
                            dformat = [
                                d.getDate().padLeft(),
                                (d.getMonth() + 1).padLeft(),
                                d.getFullYear()].join('/');
                    return dformat;
                }
            };


            $scope.userRegistration = function (nombre, apellido, apellido2, email, identificador, provincia, canton, nombreUsuario, password, password2) {
                //Realizamos todas las comprobaciones de los datos introducidos
                var alphaExp = /^[a-zA-Z]+$/;
                if (!nombre) {
                    $scope.alertSimpleError('¡El nombre no puede estar vacío!');
                    return;
                }
                if (!nombre.match(alphaExp)) {
                    $scope.alertSimpleError('¡El nombre no puede contener números o espacios!');
                    return;
                }
                if (!apellido) {
                    $scope.alertSimpleError('¡El apellido no puede estar vacío!');
                    return;
                }
                if (!apellido.match(alphaExp)) {
                    $scope.alertSimpleError('¡El apellido no puede contener números o espacios!');
                    return;
                }
                if (!email) {
                    $scope.alertSimpleError('El correo no puede estar vacío, o el correo introducido no es válido.');
                    return;
                }
                if (!identificador) {
                    $scope.alertSimpleError('¡El identificador fiscal no puede estar vacío!');
                    return;
                }
                if (identificador.length < 5) {
                    $scope.alertSimpleError('El identificador fiscal debe tener como mínimo 5 caracteres');
                    return;
                }
                if (identificador.indexOf(" ") > -1) {
                    $scope.alertSimpleError('¡El identificador no puede contener espacios!');
                    return;
                }
                if (!provincia) {
                    $scope.alertSimpleError('¡La provincia no puede estar vacía!');
                    return;
                }
                if (!canton) {
                    $scope.alertSimpleError('¡El cantón no puede estar vacío!');
                    return;
                }
                if (!nombreUsuario) {
                    $scope.alertSimpleError('¡El nombre de usuario no puede estar vacío!');
                    return;
                }
                if (nombreUsuario.length < 2) {
                    $scope.alertSimpleError('¡El nombre de usuario debe tener como mínimo 2 caracteres!');
                    return;
                }
                if (nombreUsuario.indexOf(" ") > -1) {
                    $scope.alertSimpleError('¡El nombre de usuario no puede contener espacios!');
                    return;
                }
                if (!password) {
                    $scope.alertSimpleError('¡El password no puede estar vacío!');
                    return;
                }
                if (password.length < 6) {
                    $scope.alertSimpleError('¡El password debe tener como mínimo 6 caracteres!');
                    return;
                }
                if (password.indexOf(" ") > -1) {
                    $scope.alertSimpleError('¡El password no puede contener espacios!');
                    return;
                }
                if (password !== password2) {
                    $scope.alertSimpleError('El password debe ser igual en los dos campos !');
                    return;
                }

                var JSONObject = {"nombre": nombre, "apellido": apellido, "apellido2": apellido2, "identificador": identificador, "email": email, "usuario": nombreUsuario, "password": password, "estado": "activo", "uuidDevice": uuid, "pais": "Costa Rica", "provincia": provincia, "canton": canton};
                var jsonData = JSON.stringify(JSONObject);
                //Realizamos el post en AngularJS
                $http.post(GoTapApi + "gotapUserRegistration", jsonData).
                        then(function (response) {
                            $scope.alertSimpleSuccess('Registro realizado con éxito!');
                            $scope.isRegistro = true;
                            $scope.userLogIn(nombreUsuario, password);
                        }, function (response) {
                            //miramos si el usuario/dni/correo exite, si no existe hacemos post , si existe miramos si esta activo o inactivo para permitir de nuevo el registro
                            $scope.alertSimpleError(response.data);
                        });
            };
            //})
            /*---------------------------------------------CONTROLADOR DEL SYSTEM---------------------------------------------*/

            //.controller('systemControllers', function ($scope, $ionicPopup, $timeout) {

            /*---------------------------------------------Funciones para el alertas chulas parametrizadas---------------------------------------------*/
            $scope.alertSimpleError = function (contenido) {

                var alertPopup = $ionicPopup.alert({
                    title: 'Atención',
                    template: contenido,
                    buttons: [
                        {
                            text: '<b>Aceptar</b>',
                            type: 'button-gotap',
                            onTap: function () {
                                alertPopup.close();
                            }
                        }
                    ]
                });
                $cordovaVibration.vibrate(300);
                $timeout(function () {
                    alertPopup.close();
                }, 4000);
            };

            $scope.alertSimpleSuccess = function (contenido) {

                var alertPopup = $ionicPopup.alert({
                    title: '¡Enhorabuena!',
                    template: contenido,
                    buttons: [
                        {
                            text: '<b>Aceptar</b>',
                            type: 'button-gotap',
                            onTap: function () {
                                alertPopup.close();
                            }
                        }
                    ]
                });
                $timeout(function () {
                    alertPopup.close();
                }, 4000);
            };

            /*---------------------------------------------Funciones para la navegación---------------------------------------------*/
            $scope.varActive = "active";

            $scope.cambiarSlide = function (numeroslide) {
                $scope.slideactivo = numeroslide;
                $ionicScrollDelegate.scrollTop();
                $ionicScrollDelegate.resize();
                $ionicSlideBoxDelegate.update();
            };

            $scope.slideHasChanged = function ($index) {

                $timeout(function () {
                }, 50);
                //alert($index);
                $scope.cambiarSlide($index);
                if ($index === 1) {
                    $scope.varActive = "";
                    $scope.varActive2 = "active";
                } else {
                    $scope.varActive = "active";
                    $scope.varActive2 = "";
                }
            };

            $scope.navegacion = function (posicion) {
                switch (posicion) {
                    case 0:
                        document.location = 'MisDatos.html';
                        break;
                    case 1:
                        document.location = 'Transacciones.html';
                        break;
                    case 2:
                        document.location = 'RecargaAhora.html';
                        break;
                    case 3:
                        document.location = 'MediosDePago.html';
                        break;
                    case 4:
                        document.location = 'Login.html';
                        break;
                }
            };

            $scope.GoToRegistro = function () {
                document.location = 'Registro.html';
            };

        })

        /* Pantalla inicial */
        .controller('PantallaInicialFunctions', function ($scope, $timeout) {
            $timeout(function () {
                document.location = "Login.html";
            }, 3000);
        })

        /*---------------------------------------------CONTROLADOR DE RECARGA AHORA ---------------------------------------------*/
        .controller('RecargaAhoraFunctions', function ($scope, $http, $ionicPopup, $timeout, $cordovaVibration, $ionicScrollDelegate, $ionicSlideBoxDelegate) {

            var JSONObject = {"idUsuario": localStorage.idUsuario, "password": localStorage.password};

            var jsonData = JSON.stringify(JSONObject);
            $http.post(GoTapApi + "gotapGetTarjetas", jsonData).
                    then(function (response) {

                        //Mostramos solo los últimos 4 dígitos
                        if (response.data.length > 0) {
                            var tarjetas = response.data;

                            for (var i = 0; i < tarjetas.length; i++) {
                                if (tarjetas[i].estado === "activa") {
                                    var numero = (tarjetas[i].numero).toString();
                                    tarjetas[i].numero = "••••-••••-••••-" + numero.substring(12, 16);
                                }
                            }
                            $scope.tarjetas = response.data;
                        }
                        if (response.data.length === 0) {
                            $scope.alertTarjetaEmpty('¡Primero debes añadir una tarjeta!');
                        }

                    }, function (response) {
                        $scope.alertSimpleError('¡Error al importar tarjetas!');
                    });

            var JSONObject = {"idUsuario": localStorage.idUsuario, "password": localStorage.password};

            var jsonData = JSON.stringify(JSONObject);
            $http.post(GoTapApi + "gotapGetTelefonos", jsonData).
                    then(function (response) {
                        $scope.telefonos = response.data;
                        console.log($scope.telefonos);
                    }, function (response) {
                        $scope.alertSimpleError('¡Error al importar teléfonos!');
                    });


            /*---------------------------------------------Funciones para la navegación---------------------------------------------*/
            $scope.navegacion = function (posicion) {

                switch (posicion) {
                    case 0:
                        document.location = 'MisDatos.html';
                        break;
                    case 1:
                        document.location = 'Transacciones.html';
                        break;
                    case 2:
                        document.location = 'RecargaAhora.html';
                        break;
                    case 3:
                        document.location = 'MediosDePago.html';
                        break;
                    case 4:
                        document.location = 'Login.html';
                        localStorage.clear();
                        break;
                    case 6:
                        document.location = 'AnadirTelefono.html';
                        break;

                }

            };

            $scope.alertTarjetaEmpty = function (contenido) {

                var alertPopup = $ionicPopup.alert({
                    title: 'Atención',
                    template: contenido,
                    buttons: [
                        {
                            text: '<b>Aceptar</b>',
                            type: 'button-gotap',
                            onTap: function () {
                                alertPopup.close();
                                document.location = "MediosDePago.html";
                            }
                        }
                    ]
                });
                $cordovaVibration.vibrate(300);
                $timeout(function () {
                    alertPopup.close();
                    document.location = "MediosDePago.html";
                }, 4000);
            };

            $scope.alertSimpleError = function (contenido) {

                var alertPopup = $ionicPopup.alert({
                    title: 'Atención',
                    template: contenido,
                    buttons: [
                        {
                            text: '<b>Aceptar</b>',
                            type: 'button-gotap',
                            onTap: function () {
                                alertPopup.close();

                            }
                        }
                    ]
                });
                $cordovaVibration.vibrate(300);
                $timeout(function () {
                    alertPopup.close();

                }, 4000);
            };

            $scope.addTransaction = function () {
                //Eliminamos el espacio de las tarjetas numero.replace(/ /g
                var JSONObject = {"numeroTarjeta": 5402053674092013, "localizacion": "No disponible", "estado": "Pendiente", "idUsuario": {"id": parseInt(localStorage.idUsuario), "nombre": localStorage.nombre, "apellido": localStorage.apellido, "apellido2": localStorage.apellido2, "identificador": localStorage.identificador, "email": localStorage.email, "usuario": localStorage.usuario, "password": localStorage.password, "fechaRegistro": localStorage.regisTimestamp, "estado": "activo"}, "tag": {"tag": "3wgt7f", "empresa": "Kolbi", "importe": 20.00, "descripcion": "Recarga", "moneda": "Colón"}};

                var jsonData = JSON.stringify(JSONObject);
                $http.post(GoTapApi + "gotap.transacciones", jsonData).
                        then(function (response) {

                            $scope.alertSimpleSuccess('Pago realizado con éxito');
                            location.reload();

                        }, function (response) {
                            $scope.alertSimpleError('¡El pago no ha podido realizarse!');
                            return;
                        });


            };


        })

        .controller('MediosPagoFunctions', function ($scope, $http, $ionicPopup, $timeout, $cordovaVibration, $ionicScrollDelegate, $ionicSlideBoxDelegate) {

            var JSONObject = {"idUsuario": localStorage.idUsuario, "password": localStorage.password};
            var jsonData = JSON.stringify(JSONObject);
            //obtenemos el JSON de tarjetas de credito en funcion de la provincia en caso contrario, redirigmos a la pagina de tarjetas
            $http.post(GoTapApi + "gotapGetTarjetas", jsonData).
                    then(function (response) {

                        $scope.tarjetas = response.data;

                        generaSumarios(response.data);
                        //implementar si no tiene tarjetas

                    }, function (response) {
                        $scope.alertSimpleError('¡Error al importar tarjetas!');
                        return;
                    });



            $http.post(GoTapApi + "gotapGetTelefonos", jsonData).
                    then(function (response) {
                        $scope.telefonos = response.data;
                        //function generaSumarios
                    }, function (response) {
                        $scope.alertSimpleError('¡Error al importar teléfonos!');
                        return;
                    });

            $scope.addTarjeta = function (numero, nombre, expiraEn, cvc) {

                var EsValido;
                var tipoTarjeta;
                var añoActual = new Date().getFullYear();

                if (!numero) {
                    $scope.alertSimpleError("Introduce el número de tarjeta");
                    return -1;
                }

                //Antes de nada con una Funcion de Jquery que implementa el algoritmo de Luhn miramos si la tarjeta es valida : https://github.com/PawelDecowski/jQuery-CreditCardValidator/
                $('#validarTarjeta').validateCreditCard(function (result) {
                    if (result.valid === true && result.length_valid === true && result.luhn_valid === true) {
                        EsValido = true;
                        tipoTarjeta = result.card_type.name;
                    } else {
                        EsValido = false;
                    }
                });

                if (!nombre || nombre.length === 0) {
                    $scope.alertSimpleError("¡Introduce el nombre del propietario correctamente!");
                    return -1;
                }

                if (!expiraEn || expiraEn.length < 9) {
                    $scope.alertSimpleError("¡Introduce la fecha de expiración correctamente! \n Recuerda el año son 4 dígitos.");
                    return -1;
                }
                var anyExpiracion = expiraEn.substring(5, 9);

                if (!cvc) {
                    $scope.alertSimpleError("¡Introduce el CVC!");
                    return -1;
                }

                if (!EsValido || nombre.length === 0 || expiraEn.length < 9 || cvc.length < 3 || anyExpiracion < añoActual) {
                    $scope.alertSimpleError("Esta tarjeta no es válida \n ¡Por favor introduce una que lo sea!");
                    return-1;
                }

                //Parseamos la fecha a timestamp
                var fechaForm = expiraEn.split(" / ");
                var fechaNueva = new Date(fechaForm[1], fechaForm[0], 00);



                //Eliminamos el espacio de las tarjetas numero.replace(/ /g
                var JSONObject = {"numero": parseInt(numero.replace(/ /g, "")), "nombrePropietario": nombre, "apellidoPropietario": "", "cvc": parseInt(cvc), "caducidad": formatDate(fechaNueva), "tipo": tipoTarjeta, "estado": "activa", "idUsuario": localStorage.idUsuario, "password": localStorage.password};

                var jsonData = JSON.stringify(JSONObject);
                 $http.post(GoTapApi + "gotapInsertTarjeta", jsonData).
                 then(function (response) {
                 
                 $scope.alertSimpleSuccess('Tarjeta añadida con éxito');
                 location.reload();
                 
                 }, function (response) {
                 $scope.alertSimpleError('¡No ha sido posible añadir su tarjeta!');
                 return;
                 });

            };
            $scope.alertSimpleSuccess = function (contenido) {

                var alertPopup = $ionicPopup.alert({
                    title: '¡Enhorabuena!',
                    template: contenido,
                    buttons: [
                        {
                            text: '<b>Aceptar</b>',
                            type: 'button-gotap',
                            onTap: function () {
                                alertPopup.close();
                                location.reload();
                            }
                        }
                    ]
                });
                $timeout(function () {
                    alertPopup.close();
                    location.reload();
                }, 4000);
            };

            $scope.alertSimpleError = function (contenido) {

                var alertPopup = $ionicPopup.alert({
                    title: 'Atención',
                    template: contenido,
                    buttons: [
                        {
                            text: '<b>Aceptar</b>',
                            type: 'button-gotap',
                            onTap: function () {
                                alertPopup.close();
                            }
                        }
                    ]
                });
                $timeout(function () {
                    alertPopup.close();
                }, 4000);
            };



            $scope.deleteTarjeta = function (numeroTarjeta, propietario, fecha, cvc, tipo, id) {
                //debugger;
                alert(numeroTarjeta);
                //var direccion =  "gotap.tarjetas/getAllCreditCards/"+parseInt(localStorage.idUsuario);
                //alert(direccion);
            };

            function generaSumarios(data) {
          
                for (var i = 0; i < data.length; i++) {
                    //    alert(data[i].numero);
                    // Crea las hileras de la tabla

                    if (data[i].estado === "activa") {
                        console.log(data[i]);
                        localStorage['tarjetaNumero' + i] = data[i].numero;
                        localStorage['tarjetaId' + i] = data[i].id;
                        localStorage['tarjetaPropietario' + i] = data[i].nombrePropietario;
                        localStorage['tarjetaFecha' + i] = data[i].caducidad;
                        localStorage['tarjetaCvc' + i] = data[i].cvc;
                        localStorage['tarjetaTipo' + i] = data[i].tipo;

                        //  alert(data[i].estado)
                        var puntoInsercion = document.getElementById("puntoInsercion");
                        var bloque = document.createElement("details");
                        var resumen = document.createElement("summary");
                        resumen.setAttribute('class', 'resumenTarjeta');
                        resumen.setAttribute('id', 'summary');
                        resumen.setAttribute('ng-model', "numerotarjeta");
                        var br = document.createElement("br");
                        var divList = document.createElement("div");
                        var labelPropietario = document.createElement("label");
                        labelPropietario.setAttribute('class', 'item item-input custom-item');
                        var spanPropietario = document.createElement("span");
                        spanPropietario.setAttribute('class', 'input-label');
                        spanPropietario.setAttribute('ng-model', "propietario");
                        var labelFecha = document.createElement("label");
                        labelFecha.setAttribute('class', 'item item-input custom-item');
                        var spanFecha = document.createElement("span");
                        spanFecha.setAttribute('class', 'input-label');
                        spanFecha.setAttribute('ng-model', "fecha");
                        var labelSecreto = document.createElement("label");
                        labelSecreto.setAttribute('class', 'item item-input custom-item');
                        var spanSecreto = document.createElement("span");
                        spanSecreto.setAttribute('class', 'input-label');
                        spanSecreto.setAttribute('ng-model', "cvc");
                        var labelTipo = document.createElement("label");
                        labelTipo.setAttribute('class', 'item item-input custom-item');
                        var spanTipo = document.createElement("span");
                        spanTipo.setAttribute('class', 'input-label');
                        spanTipo.setAttribute('ng-model', "tipo");
                        var botonEliminar = document.createElement("button");

                        var numero = data[i].numero.toString();
                        //  var ocular =  numero.substring(0,13).replace(/./g, '•');
                        var noOcultar = "••••-••••-••••-" + numero.substring(12, 16);

                        resumen.innerHTML = "Credit Card:  " + noOcultar;
                        bloque.appendChild(resumen);

                        spanPropietario.innerHTML = "<b>Propietario:  " + data[i].nombrePropietario + "</b>";
                        labelPropietario.appendChild(spanPropietario);
                        divList.appendChild(labelPropietario);

                        spanFecha.innerHTML = "<b>Expira el:  " + formatDate(data[i].caducidad) + "</b>";
                        labelFecha.appendChild(spanFecha);
                        divList.appendChild(labelFecha);

                        spanSecreto.innerHTML = "<b>CVC:  " + data[i].cvc + "</b>";
                        labelSecreto.appendChild(spanSecreto);
                        divList.appendChild(labelSecreto);

                        spanTipo.innerHTML = "<b>Tipo:  " + data[i].tipo + "</b>";
                        labelTipo.appendChild(spanTipo);
                        divList.appendChild(labelTipo);

                        botonEliminar.setAttribute('class', 'button button-full button-assertive');
                        //botonEliminar.setAttribute('ng-click', 'deleteTarjeta('+data[i].numero+','+data[i].propietario+','+data[i].caducidad+','+data[i].cvc+','+data[i].tipo+','+data[i].id+')');
                        botonEliminar.setAttribute('id', 'botonEliminar');
                        botonEliminar.setAttribute('onclick', 'eliminarTarjeta(' + i + ')');
                        botonEliminar.innerHTML = "Eliminar Tarjeta";
                        divList.appendChild(botonEliminar);

                        bloque.appendChild(divList);

                        // agrega la hilera al final de la tabla (al final del elemento tblbody)
                        puntoInsercion.appendChild(bloque);
                        puntoInsercion.appendChild(br);
                    }

                }


            }



            /*---------------------------------------------Funciones para la navegación---------------------------------------------*/
            $scope.navegacion = function (posicion) {

                switch (posicion) {
                    case 0:
                        document.location = 'MisDatos.html';
                        break;
                    case 1:
                        document.location = 'Transacciones.html';
                        break;
                    case 2:
                        document.location = 'RecargaAhora.html';
                        break;
                    case 3:
                        document.location = 'MediosDePago.html';
                        break;
                    case 4:
                        document.location = 'Login.html';
                        localStorage.clear();
                        break;
                    case 6:
                        document.location = 'AnadirTelefono.html';
                        break;

                }

            };


        })
        .controller('AnadirTelefono', function ($scope, $http, $ionicPopup, $timeout, $cordovaVibration, $ionicScrollDelegate, $ionicSlideBoxDelegate) {

            var JSONObject = {"idUsuario": localStorage.idUsuario, "password": localStorage.password};
            var jsonData = JSON.stringify(JSONObject);
            $http.post(GoTapApi + "gotapGetTelefonos", jsonData).
                    then(function (response) {

                        generaSumTelf(response.data);

                    }, function (response) {
                        $scope.alertSimpleError('¡Error al importar teléfonos!');

                    });

            generaSumTelf = function (data) {

                for (var i = 0; i < data.length; i++) {
                    //    alert(data[i].numero);
                    // Crea las hileras de la tabla

                    if (data[i].activo === "activo") {

                        localStorage['telefonoNumero' + i] = data[i].numero;
                        localStorage['telefonoId' + i] = data[i].id;
                        localStorage['telefonoAlias' + i] = data[i].alias;
                        localStorage['telefonoFavorito' + i] = data[i].favorito;


                        var puntoInsercion = document.getElementById("puntoInsercionNumeros");
                        var bloque = document.createElement("details");
                        var resumen = document.createElement("summary");
                        resumen.setAttribute('class', 'resumenTarjeta');
                        resumen.setAttribute('id', 'summaryNumeros');
                        var br = document.createElement("br");
                        var divList = document.createElement("div");

                        var labelAlias = document.createElement("label");
                        labelAlias.setAttribute('class', 'item item-input custom-item');
                        var spanAlias = document.createElement("span");
                        spanAlias.setAttribute('class', 'input-label');

                        var labelFavorito = document.createElement("label");
                        labelFavorito.setAttribute('class', 'item item-input custom-item');
                        var spanFavorito = document.createElement("span");
                        spanFavorito.setAttribute('class', 'input-label');

                        var botonEliminar = document.createElement("button");

                        resumen.innerHTML = "Número:  " + data[i].numero;
                        bloque.appendChild(resumen);

                        spanAlias.innerHTML = "<b>Alias:  " + data[i].alias + "</b>";
                        labelAlias.appendChild(spanAlias);
                        divList.appendChild(labelAlias);

                        spanFavorito.innerHTML = "<b>Favorito:  " + data[i].favorito + "</b>";
                        labelFavorito.appendChild(spanFavorito);
                        divList.appendChild(labelFavorito);

                        botonEliminar.setAttribute('class', 'button button-full button-assertive');
                        botonEliminar.setAttribute('onclick', 'eliminarNumero(' + i + ')');
                        botonEliminar.innerHTML = "Eliminar Número";
                        divList.appendChild(botonEliminar);

                        bloque.appendChild(divList);

                        // agrega la hilera al final de la tabla (al final del elemento tblbody)
                        puntoInsercion.appendChild(bloque);
                        puntoInsercion.appendChild(br);
                    }

                }
            };




            var idUsuario = localStorage.idUsuario;
            $scope.telefononuevo = {};

            $scope.alertSimpleError = function (contenido) {
                var alertPopup = $ionicPopup.alert({
                    title: 'Atención',
                    template: contenido,
                    buttons: [
                        {
                            text: '<b>Aceptar</b>',
                            type: 'button-gotap',
                            onTap: function () {
                                alertPopup.close();
                            }
                        }
                    ]
                });
                $timeout(function () {
                    alertPopup.close();
                }, 4000);
            };

            $scope.insertPhone = function () {
                //debugger;
                //Obtenemos los datos de la direccion, llamamos al método de la api de direcciones
                var JSONObject = {"idUsuario": idUsuario, "password": localStorage.password, "numero": $scope.telefononuevo.numero, "alias": $scope.telefononuevo.aliasnuevo, "favorito": "No", "activo": "activo"};
                var jsonData = JSON.stringify(JSONObject);
                $http.post(GoTapApi + "gotapUpdateTelefono", jsonData).
                        then(function (response) {
                            document.location = 'RecargaAhora.html';
                        }, function (response) {
                            $scope.alertSimpleError('¡Error registrando su teléfono nuevo!');
                            return;
                        });
            };


            /*---------------------------------------------Funciones para la navegación---------------------------------------------*/
            $scope.navegacion = function (posicion) {

                switch (posicion) {
                    case 0:
                        document.location = 'MisDatos.html';
                        break;
                    case 1:
                        document.location = 'Transacciones.html';
                        break;
                    case 2:
                        document.location = 'RecargaAhora.html';
                        break;
                    case 3:
                        document.location = 'MediosDePago.html';
                        break;
                    case 4:
                        document.location = 'Login.html';
                        localStorage.clear();
                        break;
                    case 6:
                        document.location = 'AnadirTelefono.html';
                        break;

                }

            };

        })
        .controller('DownLoadTransactions', function ($scope, $http, $ionicPopup, $timeout, $cordovaVibration, $ionicScrollDelegate, $ionicSlideBoxDelegate) {



            $http.get(GoTapApi + "gotap.transacciones/getAllTransactionsById/" + localStorage.idUsuario).
                    then(function (response) {

                        generaSumTransactions(response.data);

                    }, function (response) {
                        $scope.alertSimpleError('¡Error al importar teléfonos!');

                    });

            generaSumTransactions = function (data) {
                //debugger;
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {

                        var puntoInsercion = document.getElementById("puntoInsercionTransacciones");
                        var bloque = document.createElement("details");
                        var resumen = document.createElement("summary");
                        resumen.setAttribute('class', 'resumenTarjeta');
                        resumen.setAttribute('id', 'summary');

                        var br = document.createElement("br");
                        var divList = document.createElement("div");

                        var labelCantidad = document.createElement("label");
                        labelCantidad.setAttribute('class', 'item item-input custom-item');
                        var spanCantidad = document.createElement("span");
                        spanCantidad.setAttribute('class', 'input-label');

                        var labelConcepto = document.createElement("label");
                        labelConcepto.setAttribute('class', 'item item-input custom-item');
                        var spanConcepto = document.createElement("span");
                        spanConcepto.setAttribute('class', 'input-label');

                        var labelFecha = document.createElement("label");
                        labelFecha.setAttribute('class', 'item item-input custom-item');
                        var spanFecha = document.createElement("span");
                        spanFecha.setAttribute('class', 'input-label');

                        var labelLocal = document.createElement("label");
                        labelLocal.setAttribute('class', 'item item-input custom-item');
                        var spanLocal = document.createElement("span");
                        spanLocal.setAttribute('class', 'input-label');

                        var numero = data[i].numeroTarjeta.toString();
                        var ocular = numero.substring(0, 13).replace(/./g, '•');
                        var noOcultar = numero.substring(12, 17);

                        resumen.innerHTML = "Tarjeta utilizada:  " + ocular + noOcultar;
                        bloque.appendChild(resumen);

                        spanCantidad.innerHTML = "<b>Cantidad:  " + data[i].cantidad + "</b>";
                        labelCantidad.appendChild(spanCantidad);
                        divList.appendChild(labelCantidad);

                        spanConcepto.innerHTML = "<b>Concepto:  " + data[i].concepto + "</b>";
                        labelConcepto.appendChild(spanConcepto);
                        divList.appendChild(labelConcepto);

                        var fecha = $scope.formatDate(data[i].fechaTransaccion);
                        spanFecha.innerHTML = "<b>Fecha:  " + fecha + "</b>";
                        labelFecha.appendChild(spanFecha);
                        divList.appendChild(labelFecha);

                        spanLocal.innerHTML = "<b>Localización:  " + data[i].localizacion + "</b>";
                        labelLocal.appendChild(spanLocal);
                        divList.appendChild(labelLocal);

                        bloque.appendChild(divList);

                        // agrega la hilera al final de la tabla (al final del elemento tblbody)
                        puntoInsercion.appendChild(bloque);
                        puntoInsercion.appendChild(br);

                    }
                } else {
                    var puntoInsercion = document.getElementById("puntoInsercionTransacciones");
                    puntoInsercion.innerHTML = "<span style='color:white; font-weight:bold'> No hay transacciones </span>";
                }
            };
            $scope.formatDate = function (value) {
                if (value) {
                    Number.prototype.padLeft = function (base, chr) {
                        var len = (String(base || 10).length - String(this).length) + 1;
                        return len > 0 ? new Array(len).join(chr || '0') + this : this;
                    };
                    var d = new Date(value),
                            dformat = [
                                d.getDate().padLeft(),
                                (d.getMonth() + 1).padLeft(),
                                d.getFullYear()].join('/')/*+
                                 ' ' +
                                 [ d.getHours().padLeft(),
                                 d.getMinutes().padLeft(),
                                 d.getSeconds().padLeft()].join(':')*/;
                    return dformat;
                }
            };
            /*---------------------------------------------Funciones para la navegación---------------------------------------------*/
            $scope.navegacion = function (posicion) {

                switch (posicion) {
                    case 0:
                        document.location = 'MisDatos.html';
                        break;
                    case 1:
                        document.location = 'Transacciones.html';
                        break;
                    case 2:
                        document.location = 'RecargaAhora.html';
                        break;
                    case 3:
                        document.location = 'MediosDePago.html';
                        break;
                    case 4:
                        document.location = 'Login.html';
                        localStorage.clear();
                        break;
                    case 5:
                        document.location = 'EditarDireccion.html';
                        break;
                    case 6:
                        document.location = 'AnadirTelefono.html';
                        break;

                }
            };
        })
        .controller('MisDatos', function ($scope, $http, $ionicPopup, $timeout, $cordovaVibration, $ionicScrollDelegate, $ionicSlideBoxDelegate, $ionicActionSheet) {

            $scope.provincias = [{provincia: "San José"}, {provincia: "Alajuela"}, {provincia: "Cartago"}, {provincia: "Heredia"}, {provincia: "Limón"}, {provincia: "Guanacaste"}, {provincia: "Puntarenas"}];
            $scope.getCantones = function (provincia) {
                switch (provincia) {
                    case "San José":
                        $scope.cantones = [{canton: "San José"}, {canton: "Escazú"}, {canton: "Desamparados"}, {canton: "Puriscal"}, {canton: "Tarrazú"}, {canton: "Aserrí"}, {canton: "Mora"}, {canton: "Goicoechea"}, {canton: "Santa Ana"}, {canton: "Alajuelita"}, {canton: "Vásquez de Coronado"}, {canton: "Acosta"}, {canton: "Tibás"}, {canton: "Moravia"}, {canton: "Montes de Oca"}, {canton: "Turrubares"}, {canton: "Dota"}, {canton: "Curridabat"}, {canton: "Pérez Zeledón"}, {canton: "León Cortés Castro"}];
                        break;
                    case "Alajuela":
                        $scope.cantones = [{canton: "Alajuela"}, {canton: "San Ramón"}, {canton: "Grecia"}, {canton: "San Mateo"}, {canton: "Atenas"}, {canton: "Naranjo"}, {canton: "Palmares"}, {canton: "Poás"}, {canton: "Orotina"}, {canton: "San Carlos"}, {canton: "Zarcero"}, {canton: "Valverde Vega"}, {canton: "Upala"}, {canton: "Los Chiles"}, {canton: "Guatuso"}];
                        break;
                    case "Cartago":
                        $scope.cantones = [{canton: "Cartago"}, {canton: "Paraíso"}, {canton: "La unión"}, {canton: "Jimenez"}, {canton: "Turriabla"}, {canton: "Alvarado"}, {canton: "Oreamuno"}, {canton: "El Guarco"}];
                        break;
                    case "Heredia":
                        $scope.cantones = [{canton: "Heredia"}, {canton: "Barva"}, {canton: "Santo Domingo"}, {canton: "Santa Bárbara"}, {canton: "San Rafael"}, {canton: "San Isidro"}, {canton: "Belén"}, {canton: "Flores"}, {canton: "San Pablo"}, {canton: "Sarapiquí"}];
                        break;
                    case "Limón":
                        $scope.cantones = [{canton: "Limón"}, {canton: "Pococí"}, {canton: "Siquirres"}, {canton: "Talamanca"}, {canton: "Matina"}, {canton: "Guácimo"}];
                    case "Guanacaste":
                        $scope.cantones = [{canton: "Liberia"}, {canton: "Nicoya"}, {canton: "Santa Cruz"}, {canton: "Bagaces"}, {canton: "Carrillo"}, {canton: "Cañas"}, {canton: "Abangares"}, {canton: "Tilarán"}, {canton: "Nandayure"}, {canton: "La Cruz"}, {canton: "Hojancha"}];
                    case "Puntarenas":
                        $scope.cantones = [{canton: "Puntarenas"}, {canton: "Esparza"}, {canton: "Buenos Aires"}, {canton: "Montes de Oro"}, {canton: "Osa"}, {canton: "Quepos"}, {canton: "Golfito"}, {canton: "Coto Brus"}, {canton: "Parrita"}, {canton: "Corredores"}, {canton: "Garabito"}];
                        break;
                    default:
                }
            };

            var JSONObject = {"idUsuario": localStorage.idUsuario, "password": localStorage.password};
            var jsonData = JSON.stringify(JSONObject);
            console.log(jsonData);

            $http.post(GoTapApi + "gotapGetTelefonos", jsonData).
                    then(function (response) {

                        $scope.telefonos = response.data;
                        $scope.telefonoRegistro = response.data[0].numero;

                    }, function (response) {
                        console.log(response);
                        $scope.alertSimpleError('¡Error al importar el número de teléfono!');
                        return;
                    });

            //debugger;
            //Asignamos los valores guardados en localStorage a la pantalla
            $scope.idUsuario = localStorage.idUsuario;
            $scope.nombre = localStorage.nombre;
            $scope.apellido = localStorage.apellido;
            $scope.apellido2 = localStorage.apellido2;
            $scope.identificador = localStorage.identificador;
            $scope.email = localStorage.email;
            $scope.usuario = localStorage.usuario;
            $scope.realpassword = localStorage.password;
            $scope.password = localStorage.password.replace(/./g, '•');//CUANDO SE ESTE PROBANDO ESTA PANTALLA NO REFRESCAR PORQUE SE PIERDE LA CONTRA !
            $scope.fechaRegistro = localStorage.fechaRegistro;
            $scope.provincia = localStorage.provinciaEstado;
            $scope.getCantones($scope.provincia);
            $scope.canton = localStorage.cantonCiudad;


            $scope.updateDireccion = function (provincia, canton) {

                var JSONObject;
                JSONObject = {"nombre": localStorage.nombre, "apellido": localStorage.apellido, "apellido2": localStorage.apellido2, "email": localStorage.email, "usuario": localStorage.usuario, "password": localStorage.password, "provincia": provincia, "canton": canton, "estado": "activo", "idUsuario": localStorage.idUsuario, "passwordUser": localStorage.password};
                var jsonData = JSON.stringify(JSONObject);

                $http.post(GoTapApi + "gotapUserChange", jsonData).
                        then(function (response) {
                            localStorage.nombre = $scope.nombre;
                            localStorage.apellido = $scope.apellido;
                            localStorage.apellido2 = $scope.apellido2;
                            localStorage.email = $scope.email;
                            localStorage.usuario = $scope.usuario;
                            if ($scope.userPass) {
                                localStorage.password = $scope.userPass;
                            }
                            $scope.alertSimpleSuccess('Tus modificaciones han sido realizadas');
                        }, function (response) {
                            $scope.alertSimpleError('No ha sido posible realizar tus modificaciones');
                            //document.location='MisDatos.html'
                        });
                localStorage.provinciaEstado = provincia;
                localStorage.cantonCiudad = canton;
            };


            $scope.alertSimpleSuccess = function (contenido) {

                var alertPopup = $ionicPopup.alert({
                    title: '¡Enhorabuena!',
                    template: contenido,
                    buttons: [
                        {
                            text: '<b>Aceptar</b>',
                            type: 'button-gotap',
                            onTap: function () {
                                alertPopup.close();
                                location.reload();
                            }
                        }
                    ]
                });
                $timeout(function () {
                    alertPopup.close();
                    location.reload();
                }, 4000);
            };

            $scope.alertBaja = function (contenido) {

                var alertPopup = $ionicPopup.alert({
                    title: '¡Adiós!',
                    template: contenido,
                    buttons: [
                        {
                            text: '<b>Aceptar</b>',
                            type: 'button-gotap',
                            onTap: function () {
                                alertPopup.close();
                                document.location = "Login.html";
                            }
                        }
                    ]
                });
                $timeout(function () {
                    alertPopup.close();
                    document.location = "Login.html";
                }, 4000);
            };

            $scope.alertSimpleError = function (contenido) {

                var alertPopup = $ionicPopup.alert({
                    title: 'Atención',
                    template: contenido,
                    buttons: [
                        {
                            text: '<b>Aceptar</b>',
                            type: 'button-gotap',
                            onTap: function () {
                                alertPopup.close();
                                location.reload();
                            }
                        }
                    ]
                });
                $cordovaVibration.vibrate(300);
                $timeout(function () {
                    alertPopup.close();
                    location.reload();
                }, 4000);
            };


            /*---------------------------------------------Funciones Para la edicion de los datos---------------------------------------------*/

            $scope.showActionsheet = function (numero) {

                switch (numero) {
                    case 1:
                        elemento = ' nombre';
                        $scope.showSheet(elemento);
                        label = 1;
                        break;
                    case 2:
                        elemento = ' apellidos';
                        $scope.showSheet(elemento);
                        label = 2;
                        break;
                    case 3:
                        elemento = ' segundo apellido';
                        $scope.showSheet(elemento);
                        label = 3;
                        break;
                    case 4:
                        $scope.alertSimpleError('¡El identificador no se puede cambiar!');
                        break;
                    case 5:
                        elemento = ' email ';
                        $scope.showSheet(elemento);
                        label = 5;
                        break;
                    case 6:
                        elemento = ' Usuario';
                        $scope.showSheet(elemento);
                        label = 6;
                        break;
                    case 7:
                        elemento = ' Password';
                        $scope.showSheet(elemento);
                        label = 7;
                        break;
                    case 8:
                        $scope.alertSimpleError('¡La fecha de registro no se puede cambiar!');
                        label = 8;
                        break;
                    case 9:
                        $scope.alertSimpleError('¡Los números deben gestionarse desde el menú de teléfonos!');
                        //  label=9;
                        break;
                    case 10:
                        var hideSheet = $ionicActionSheet.show({
                            titleText: '¿Seguro que quieres darte de baja?',
                            buttons: [
                                {text: '<b><i class="icon ion-edit ion-trash-a assertive"></i></b> <b>¡Dame de baja!</b>'},
                                {text: '<i class="icon ion-ios-close-outline gotap"></i> Cancelar'}
                            ],
                            buttonClicked: function (index) {
                                console.log('BUTTON CLICKED', index);
                                if (index === 0) {
                                    JSONObject = {"id": parseInt(localStorage.idUsuario), "nombre": $scope.nombre, "apellido": $scope.apellido, "apellido2": $scope.apellido2, "identificador": localStorage.identificador, "email": $scope.email, "usuario": $scope.usuario, "password": localStorage.password, "fechaRegistro": localStorage.regisTimestamp, "estado": "inactivo", "uuidDevice": localStorage.uuid, "pais": "Costa Rica", "provinciaEstado": $scope.provincia, "cantonCiudad": $scope.canton, "direccion": "", "codigoPostal": ""};
                                    var jsonData = JSON.stringify(JSONObject);
                                    $http.put(GoTapApi + "gotap.usuarios/" + parseInt(localStorage.idUsuario), jsonData).
                                            then(function (response) {
                                                $scope.alertBaja('Has sido dado de baja.\n Esperamos que esto no sea una despedida');

                                            }, function (response) {
                                                $scope.alertSimpleError('No ha sido posible realizar tus modificaciones');
                                                //document.location='MisDatos.html'
                                            });
                                } else {
                                    hideSheet();
                                }
                            }
                        });
                        $timeout(function () {
                            hideSheet();
                        }, 4000);
                }
            };

            $scope.showSheet = function (elemento) {


                var hideSheet = $ionicActionSheet.show({
                    titleText: '¿Deseas cambiar tu' + elemento + '?',
                    buttons: [
                        {text: '<i class="icon ion-edit custom-icon-sheet"></i> Editar'},
                        {text: '<i class="icon ion-ios-close-outline assertive"></i> Cancelar'}
                    ],
                    //  destructiveText: 'Eliminar',
                    /*  cancelText: 'Cancelar',
                     cancel: function() {
                     console.log('CANCELLED');
                     },*/
                    buttonClicked: function (index) {
                        console.log('BUTTON CLICKED', index);

                        if (index === 0) {

                            $scope.doEdit();
                            hideSheet();

                        } else {

                            hideSheet();

                        }

                    }/*
                     destructiveButtonClicked: function() {
                     console.log('DESTRUCT');
                     return true;
                     }*/

                });
                $timeout(function () {
                    hideSheet();
                }, 4000);
            };


            $scope.doEdit = function () {

                $scope.data = {};

                // An elaborate, custom popup
                var myPopup = $ionicPopup.show({
                    template: '<input type="text" ng-model="data.wifi">',
                    title: 'Introduce tu nuevo' + elemento,
                    //subTitle: nombre,
                    scope: $scope,
                    buttons: [
                        {text: 'Cancelar'},
                        {
                            text: '<b>Guardar</b>',
                            type: 'button-balanced',
                            onTap: function (e) {
                                if (!$scope.data.wifi) {
                                    //don't allow the user to close unless he enters wifi password
                                    e.preventDefault();
                                } else {
                                    console.log(label);
                                    switch (label) {


                                        case 1:
                                            $scope.nombre = $scope.data.wifi;
                                            break;
                                        case 2:
                                            $scope.apellido = $scope.data.wifi;
                                            break;
                                        case 3:
                                            $scope.apellido2 = $scope.data.wifi;
                                            console.log($scope.apellido2);
                                            break;
                                        case 5:
                                            $scope.email = $scope.data.wifi;
                                            break;
                                        case 6:
                                            $scope.usuario = $scope.data.wifi;
                                            break;
                                        case 7:
                                            $scope.userPass = $scope.data.wifi;
                                            console.log($scope.userPass);
                                            break;
                                            /*  case 9:
                                             $scope.telfUser = $scope.data.wifi;
                                             break;*/
                                        default:
                                            break;
                                    }
                                    $scope.updateUser();

                                }

                            }
                        }
                    ]
                });

            };

            $scope.updateUser = function () {
                //debugger;
                //Realizamos todas las comprobaciones de los datos introducidos
                var alphaExp = /^[a-zA-Z]+$/;
                if (!$scope.nombre) {
                    $scope.alertSimpleError('¡El nombre no puede estar vacío!');
                    return;
                }
                if (!$scope.nombre.match(alphaExp)) {
                    $scope.alertSimpleError('¡El nombre no puede contener números o espacios!');
                    return;
                }
                if (!$scope.apellido) {
                    $scope.alertSimpleError('¡El apellido no puede estar vacío!');
                    return;
                }
                if (!$scope.apellido.match(alphaExp)) {
                    $scope.alertSimpleError('¡El apellido no puede contener números o espacios!');
                    return;
                }
                if (!$scope.email) {
                    $scope.alertSimpleError('El correo no puede estar vacío, o el correo introducido no es válido.');
                    return;
                }
                if (!$scope.usuario) {
                    $scope.alertSimpleError('¡El nombre de usuario no puede estar vacío!');
                    return;
                }
                if ($scope.usuario.length < 2) {
                    $scope.alertSimpleError('¡El nombre de usuario debe tener como mínimo 2 caracteres!');
                    return;
                }
                if ($scope.usuario.indexOf(" ") > -1) {
                    $scope.alertSimpleError('¡El nombre de usuario no puede contener espacios!');
                    return;
                }

                if (!$scope.userPass && !$scope.realpassword) {
                    $scope.alertSimpleError('¡El password no puede estar vacío!');
                    return;

                    if ($scope.userPass !== null) {
                        if ($scope.userPass.length < 6) {
                            $scope.alertSimpleError('¡El password debe tener como mínimo 6 caracteres!');
                            return;
                        }
                        if ($scope.userPass.indexOf(" ") > -1) {
                            $scope.alertSimpleError('¡El password no puede contener espacios!');
                            return;
                        }
                    }

                    var JSONObject;

                    if (!$scope.userPass) {

                        JSONObject = {"nombre": localStorage.nombre, "apellido": localStorage.apellido, "apellido2": localStorage.apellido2, "email": localStorage.email, "usuario": localStorage.usuario, "password": localStorage.password, "provincia": provincia, "canton": canton, "estado": "activo", "idUsuario": localStorage.idUsuario, "passwordUser": localStorage.password};

                    } else {

                        JSONObject = {"nombre": localStorage.nombre, "apellido": localStorage.apellido, "apellido2": localStorage.apellido2, "email": localStorage.email, "usuario": localStorage.usuario, "password": $scope.userPass, "provincia": provincia, "canton": canton, "estado": "activo", "idUsuario": localStorage.idUsuario, "passwordUser": localStorage.password};

                    }
                    console.log(JSONObject);

                    var jsonData = JSON.stringify(JSONObject);
                    $http.post(GoTapApi + "gotapUserChange", jsonData).
                            then(function (response) {
                                localStorage.nombre = $scope.nombre;
                                localStorage.apellido = $scope.apellido;
                                localStorage.apellido2 = $scope.apellido2;
                                localStorage.email = $scope.email;
                                localStorage.usuario = $scope.usuario;
                                localStorage.password = $scope.userPass;
                                $scope.alertSimpleSuccess('Tus modificaciones han sido realizadas');
                            }, function (response) {
                                $scope.alertSimpleError('No ha sido posible realizar tus modificaciones');
                                //document.location='MisDatos.html'
                            });

                }
                $scope.alertSimpleSuccess = function (contenido) {

                    var alertPopup = $ionicPopup.alert({
                        title: '¡Enhorabuena!',
                        template: contenido,
                        buttons: [
                            {
                                text: '<b>Aceptar</b>',
                                type: 'button-gotap',
                                onTap: function () {
                                    alertPopup.close();
                                    location.reload();
                                }
                            }
                        ]
                    });
                    $timeout(function () {
                        alertPopup.close();
                        location.reload();
                    }, 4000);
                };

                $scope.alertBaja = function (contenido) {

                    var alertPopup = $ionicPopup.alert({
                        title: '¡Adiós!',
                        template: contenido,
                        buttons: [
                            {
                                text: '<b>Aceptar</b>',
                                type: 'button-gotap',
                                onTap: function () {
                                    alertPopup.close();
                                    document.location = "Login.html";
                                }
                            }
                        ]
                    });
                    $timeout(function () {
                        alertPopup.close();
                        document.location = "Login.html";
                    }, 4000);
                };

                $scope.alertSimpleError = function (contenido) {

                    var alertPopup = $ionicPopup.alert({
                        title: 'Atención',
                        template: contenido,
                        buttons: [
                            {
                                text: '<b>Aceptar</b>',
                                type: 'button-gotap',
                                onTap: function () {
                                    alertPopup.close();
                                    location.reload();
                                }
                            }
                        ]
                    });
                    $cordovaVibration.vibrate(300);
                    $timeout(function () {
                        alertPopup.close();
                        location.reload();
                    }, 4000);
                };

                /*---------------------------------------------Funciones para la navegación---------------------------------------------*/
                $scope.navegacion = function (posicion) {

                    switch (posicion) {
                        case 0:
                            document.location = 'MisDatos.html';
                            break;
                        case 1:
                            document.location = 'Transacciones.html';
                            break;
                        case 2:
                            document.location = 'RecargaAhora.html';
                            break;
                        case 3:
                            document.location = 'MediosDePago.html';
                            break;
                        case 4:
                            document.location = 'Login.html';
                            localStorage.clear();
                            break;
                        case 6:
                            document.location = 'AnadirTelefono.html';
                            break;
                    }
                };
            };
        }
        )
        .controller('ValidarNumeroFunctions', function ($scope, $http, $ionicPopup, $timeout, $cordovaVibration, $ionicLoading, $cordovaDevice) {

            var JSONObject = {"idUsuario": localStorage.idUsuario, "password": localStorage.password};

            var jsonData = JSON.stringify(JSONObject);
            $http.post(GoTapApi + "gotapGetTelefonos", jsonData).
                    then(function (response) {

                        $scope.telefonoRegistro = response.data[0].numero;

                    }, function (response) {
                        $scope.alertSimpleError('La validación no puede llevarse a cabo en este momento\n Por favor inténtalo de nuevo más tarde');
                        return;
                    });


            var globalRandom = "0";

            $scope.SendPhoneRandom = function (phoneNumber) {

                localStorage.numeroRegistro = phoneNumber;

                randomSix = Math.floor(100000 + Math.random() * 900000);
                globalRandom = randomSix;

                if (phoneNumber) {
                    numero = phoneNumber.toString();
                    if (numero.length === 9) {
                        $http.get("http://appsrv01.sevenandseven.net:28082/SendSMS/+34" + numero + "/" + randomSix.toString()).
                                then(function (response) {
                                }, function (response) {
                                    $scope.alertSimpleError('¡Error al enviar SMS!');
                                });
                        $ionicLoading.show({
                            template: 'Enviando...',
                            duration: 4000
                        });
                    } else if (numero.length === 8) {
                        $http.get("http://appsrv01.sevenandseven.net:28082/SendSMS/+506" + numero + "/" + randomSix.toString()).
                                then(function (response) {
                                }, function (response) {
                                    $scope.alertSimpleError('¡Error al enviar SMS!');
                                });
                        $ionicLoading.show({
                            template: 'Enviando...',
                            duration: 4000
                        });
                    } else {
                        $scope.alertSimpleError('El número debe tener 8 dígitos');
                    }
                } else {

                    $scope.alertSimpleError('¡El número es obligatorio!');
                }

            };

            $scope.VerifyAndSend = function (phoneNumber) {
                randomSix = Math.floor(100000 + Math.random() * 900000);
                globalRandom = randomSix;
                if (phoneNumber) {
                    numero = phoneNumber.toString();
                    numeroRegistro = $scope.telefonoRegistro.toString();
                    if (numeroRegistro === numero) {
                        if (numero.length === 9) {
                            $http.get("http://appsrv01.sevenandseven.net:28082/SendSMS/+34" + numero + "/" + randomSix.toString()).
                                    then(function (response) {
                                    }, function (response) {
                                        $scope.alertSimpleError('¡Error al enviar SMS!');
                                    });
                            $ionicLoading.show({
                                template: 'Enviando...',
                                duration: 4000
                            });
                        } else if (numero.length === 8) {
                            $http.get("http://appsrv01.sevenandseven.net:28082/SendSMS/+506" + numero + "/" + randomSix.toString()).
                                    then(function (response) {
                                    }, function (response) {
                                        $scope.alertSimpleError('¡Error al enviar SMS!');
                                    });
                            $ionicLoading.show({
                                template: 'Enviando...',
                                duration: 4000
                            });
                        } else {
                            $scope.alertSimpleError('El número debe tener 8 dígitos');
                        }
                    } else {
                        $scope.alertSimpleError('¡El Número debe ser el mismo con el que se registró');
                    }
                } else {
                    $scope.alertSimpleError('¡El número es obligatorio!');
                }
            };

            $scope.ValidatePhoneRandom = function (codeNumber) {
                if (globalRandom === codeNumber) {

                    var JSONObject = {"idUsuario": localStorage.idUsuario, "password": localStorage.password, "numero": parseInt(localStorage.numeroRegistro), "alias": "Mi teléfono", "favorito": "Sí", "activo": "activo"};
                    var jsonData = JSON.stringify(JSONObject);
                    $http.post(GoTapApi + "gotapUpdateTelefono", jsonData).
                            then(function (response) {
                                document.location = 'RecargaAhora.html';
                            }, function (response) {
                                $scope.alertSimpleError('¡Error Registrando su teléfono!');
                                return;
                            });
                    ;

                } else {
                    if (codeNumber) {
                        codeNumber = codeNumber.toString();
                        if (codeNumber.length === 6) {
                            $scope.alertSimpleError("¡El código no es correcto!");
                        } else {
                            $scope.alertSimpleError("¡El código debe tener 6 dígitos!");
                        }
                    } else {
                        $scope.alertSimpleError("¡El código no puede estar vacío!");
                    }
                }
            };

            $scope.ValidateAndUpdate = function (codeNumber) {
                var uuid = "null";
                document.addEventListener("deviceready", function () {
                    uuid = $cordovaDevice.getUUID();
                }, false);
                if (globalRandom === codeNumber) {

                    var JSONObject;
                    if (!$scope.userPass) {
                        JSONObject = {"idUsuario": localStorage.idUsuario, "nombre": localStorage.nombre, "apellido": localStorage.apellido, "apellido2": localStorage.apellido2, "identificador": localStorage.identificador, "email": localStorage.email, "usuario": localStorage.usuario, "password": localStorage.password, "passwordUser": localStorage.password, "fechaRegistro": localStorage.regisTimestamp, "estado": "activo", "uuidDevice": uuid, "pais": "Costa Rica", "provinciaEstado": localStorage.provinciaEstado, "cantonCiudad": localStorage.cantonCiudad, "direccion": "", "codigoPostal": ""};
                    }
                    console.log(localStorage.idUsuario);
                    console.log(JSONObject);
                    var jsonData = JSON.stringify(JSONObject);
                    $http.post(GoTapApi + "gotapUserChange", jsonData).
                            then(function (response) {

                                $scope.alertSimpleSuccess('Ahora ya puedes utilizar GoTap en este teléfono');
                                setTimeout(document.location = "Login.html", 3000);

                            }, function (response) {
                                console.log(response);
                                $scope.alertSimpleError('No ha sido posible actualizar tu teléfono\n Por favor revisa tu conexión e inténtalo de nuevo');

                            });


                } else {
                    if (codeNumber) {
                        codeNumber = codeNumber.toString();
                        if (codeNumber.length === 6) {
                            $scope.alertSimpleError("¡El código no es correcto!");
                        } else {
                            $scope.alertSimpleError("¡El código debe tener 6 dígitos!");
                        }
                    } else {
                        $scope.alertSimpleError("¡El código no puede estar vacío!");
                    }
                }
            };
            $scope.alertSimpleError = function (contenido) {

                var alertPopup = $ionicPopup.alert({
                    title: 'Atención',
                    template: contenido,
                    buttons: [
                        {
                            text: '<b>Aceptar</b>',
                            type: 'button-gotap',
                            onTap: function () {
                                alertPopup.close();
                            }
                        }
                    ]
                });
                $cordovaVibration.vibrate(300);
                $timeout(function () {
                    alertPopup.close();
                }, 4000);
            };
            $scope.alertSimpleSuccess = function (contenido) {

                var alertPopup = $ionicPopup.alert({
                    title: '¡Enhorabuena!',
                    template: contenido,
                    buttons: [
                        {
                            text: '<b>Aceptar</b>',
                            type: 'button-gotap',
                            onTap: function () {
                                alertPopup.close();
                            }
                        }
                    ]
                });
                $timeout(function () {
                    alertPopup.close();
                }, 4000);
            };
        }
        )

        .directive('creditCardForm', creditCardFormDirective)
        .directive('creditCardContainer', creditCardContainerDirective);

function creditCardFormDirective() {
    return {
        restrict: 'A',
        scope: true,
        controller: function () {
            var ctrl = this;

            ctrl.fields = {};

            ctrl.setField = function (name, element) {
                ctrl.fields[name] = element;
            };
        },
        link: function (scope, element, attrs, ctrl) {
            // TODO check for ctrl.fields presence
            var card = new Card({
                form: element[0],
                container: ctrl.fields.container[0],
                formSelectors: {
                    numberInput: '[credit-card-number]',
                    expiryInput: '[credit-card-expiry]',
                    cvcInput: '[credit-card-cvc]',
                    nameInput: '[credit-card-name]'
                },
                debug: true
            });
        }
    };
}

function creditCardContainerDirective() {
    return {
        restrict: 'EA',
        require: '^creditCardForm',
        link: function (scope, element, attrs, ccForm) {
            ccForm.setField('container', element);
        }
    };
}
