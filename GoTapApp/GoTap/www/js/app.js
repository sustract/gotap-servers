// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
  /*
.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    //IONIC YA HA CARGADO COMPLETAMENTE
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
  if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });//fin del modulo principal de angular ; Los modulos de angular nos permiten devidir nuestra app por funcionalidadesx
});
*/

angular.module('nfcPay', ['ionic', 'ngCordova', 'nfcPay.controllers' ,'nfcFilters'])

    .controller('MainController', function ($scope, nfcService) {

        $scope.tag = nfcService.tag;
        $scope.clear = function() {
            nfcService.clearTag();
        };

    })

    .factory('nfcService', function ($rootScope, $ionicPlatform) {

        var tag = {};

        $ionicPlatform.ready(function() {
            nfc.addNdefListener(function (nfcEvent) {
                console.log(JSON.stringify(nfcEvent.tag, null, 4));
                $rootScope.$apply(function(){
                    angular.copy(nfcEvent.tag, tag);
                    // if necessary $state.go('some-route')
                });
            }, function () {
                console.log("Listening for NDEF Tags.");
            }, function (reason) {
                alert("Error adding NFC Listener " + reason);
            });

        });

        return {
            tag: tag,

            clearTag: function () {
                angular.copy({}, this.tag);
            }
        };
    });
