/*
 * Autor @CarloOSX
 */

/* global Function, constructor, angular para poder hacer extends si es necesario */

Function.prototype.extends = function (superclass) {
    var constructor = this.prototype.constructor;
    this.prototype = Object.create(superclass.prototype);
    this.prototype.constructor = constructor;
    return this;
};

/*Definimos el modulo al que pertenecemos*/
angular.module('Gotap')

        /*Definimos el controlador que vamos a usar*/
        .controller('RegistroController', function (IonicFunctions, /*Starting with Angular injection*/ $scope, $state, $http, $ionicLoading) {
            /**
             * Variables globales y objetos
             */
            
            /*declaramos el objeto ionic con el que podremos acceder a los "métodos" definidas en su clase*/
            utils = new IonicFunctions();
            $scope.utils = utils;            
            $scope.provincias = [{provincia: "San José"}, {provincia: "Alajuela"}, {provincia: "Cartago"}, {provincia: "Heredia"}, {provincia: "Limón"}, {provincia: "Guanacaste"}, {provincia: "Puntarenas"}];
            /*---------------------------------------------Funciones para el Registro y Login---------------------------------------------*/
            
            //Redirect a login
            $scope.goToPage = function (pagina) {
                document.location = '#/' + pagina;
            };
             
            //Redirect a login
             
            $scope.Save1 = function (nombre, apellido, identificador, provincia, canton) {
                //Vienen juntos en apellido
                var apellidos, apellido2;
                 if(typeof(apellido) != "undefined") {
                     apellidos = apellido.split(" ");
                     apellido = apellidos[0];
                    if(typeof(apellidos[1]) != "undefined") apellido2 = apellidos[1];
                 }
                                
                console.log(nombre+ apellido + apellido2 + identificador + provincia +canton)
                var alphaExp = /^[a-zA-Z ñáéíóú]+$/;
                if (!nombre) {
                    utils.alertError('Por favor, añade un nombre');
                } else if (!nombre.match(alphaExp)) {
                    utils.alertError('El nombre no puede contener números o espacios');
                } else if (!apellido) {
                    utils.alertError('Por favor, añade un apellido');
                } else if (!apellido.match(alphaExp)) {
                    utils.alertError('El apellido no puede contener números o espacios');
                } else if (!identificador) {
                    utils.alertError('Por favor, añade un identificacor fiscal');
                } else if (identificador.length < 5) {
                    utils.alertError('El identificador fiscal debe tener como mínimo 5 caracteres');
                } else if (identificador.indexOf(" ") > -1) {
                    utils.alertError('El identificador no puede contener espacios');
                } else if (!provincia) {
                    utils.alertError('Por favor, selecciona una provincia');
                } else if (!canton) {
                    utils.alertError('Por favor, selecciona un cantón');
                    
                } else{
         
                    localStorage.RegistroNombre = nombre;
                    localStorage.RegistroApellido = apellido;
                    localStorage.RegistroApellido2 = apellido2;
                    localStorage.RegistroIdentificador = identificador;
                    localStorage.RegistroProvincia = provincia;
                    localStorage.RegistroCanton = canton;
                    
                    utils.goToPage('registro2');
                }
            };
            
            $scope.Save2 = function (nombreUsuario, password, password2, email, telefono) {
                if (!nombreUsuario) {
                    utils.alertError('Por favor, añade un nombre de usuario');
                } else if (nombreUsuario.length < 2) {
                    utils.alertError('El nombre de usuario debe tener como mínimo 2 caracteres');
                } else if (nombreUsuario.indexOf(" ") > -1) {
                    utils.alertError('El nombre de usuario no puede contener espacios');
                } else if (!password) {
                    utils.alertError('Por favor, añade una contraseña');
                } else if (password.length < 6) {
                    utils.alertError('El password debe tener como mínimo 6 caracteres');
                } else if (password.indexOf(" ") > -1) {
                    utils.alertError('El password no puede contener espacios');
                } else if (password !== password2) {
                    utils.alertError('l password debe ser igual en los dos campos');
                } else if (!email) {
                    utils.alertError('Por favor, revisa la dirección de correo introducida');
                }
            
                    localStorage.RegistroNombreUsuario = nombreUsuario;
                    localStorage.RegistroPassword = password;
                    localStorage.RegistroEmail = email;
                    localStorage.RegistroTelefono = telefono;
                    localStorage.numeroRegistro = telefono;
                    
                var JSONObject = {"nombre": localStorage.RegistroNombre, "apellido": localStorage.RegistroApellido, "apellido2": localStorage.RegistroApellido2, "identificador": localStorage.RegistroIdentificador, "email": localStorage.RegistroEmail, "usuario": localStorage.RegistroNombreUsuario, "password": localStorage.RegistroPassword,  "uuidDevice": utils.getUUID(), "pais": "Costa Rica", "provincia": localStorage.RegistroProvincia, "canton": localStorage.RegistroCanton};
                                
                    //globalRandom = randomSix;
                    if (telefono) {
                        //numero = telefono.toString();
                        //if (numero.length === 9) {
                            /*$http.get("http://appsrv01.sevenandseven.net:28082/SendSMS/+34" + numero + "/" + randomSix.toString()).
                                    then(function (response) {
                                    }, function (response) {
                                        utils.alertError('¡Error al enviar SMS!');
                                    });
                            $ionicLoading.show({
                                template: 'Enviando código mediante sms ...',
                                duration: 4500
                            });*/
                            //$scope.enviarCodigo(34, numero);
                        
                        //} else if (numero.length === 8) {
                        //$http.get("http://appsrv01.sevenandseven.net:28082/SendSMS/+506" + numero + "/" + randomSix.toString()).
                            /*       then(function (response) {
                                    }, function (response) {
                                        utils.alertError('¡Error al enviar SMS!');
                                    });
                            $ionicLoading.show({
                                template: 'Enviando código...',
                                duration: 4500
                            });*/
                        utils.postHttp("gotapUserRegistration", JSONObject).then(function (resolvedVal) {
                            console.log(JSONObject);
                            if (resolvedVal.status === 500) {
                                utils.alertError(resolvedVal.data);
                            } else {
                                //No podemos borrar el storage o perdemos los datos
                                //localStorage.clear();
                                $scope.enviarCodigo(telefono);
                                //utils.goToPage('registro4');
                                //$state.go('bienvenido');
                            }
                        });
                        //} else {
                            //utils.alertError('El celular debe ser de 8 dígitos');
                        //}
                    } else {
                        utils.alertError('Introduce el número de celular');
                    }
                
              
            };
            
            $scope.enviarCodigo = function (telefono){
                var randomSix, numero, prefijo;
                randomSix = Math.floor(100000 + Math.random() * 900000);
                //randomSix = 66666;
                localStorage.random = randomSix;
                numero = telefono.toString();
                
                if(numero.length == 9) { prefijo = "+34"; }
                if(numero.length == 8) { prefijo ="+506"; }
                
                if(prefijo){

  
                   /*return $http({
                        url: "http://appsrv01.sevenandseven.net:28082/SendSMS/" + prefijo + numero + "/" + randomSix.toString(),
                        method: "GET"
                        }).success(function(response){
                            $ionicLoading.show({
                                template: 'Enviando código...',
                                //template: 'Desactivado: introduce 555...',
                                duration: 4500
                            });
                             utils.goToPage('registro3');
                        })
                        .error(function(response){
                           utils.alertError('¡Error al enviar SMS!'); 
                        });*/
                    $http.get("http://appsrv01.sevenandseven.net:28082/SendSMS/" + prefijo + numero + "/" + randomSix.toString()).
                         then(function (response) {
                             
                         }, function (response) {
                            //utils.alertError('¡Error al enviar SMS!');
                         });
                        $ionicLoading.show({
                            template: 'Enviando código...',
                            //template: 'Desactivado: introduce 555...',
                            duration: 3500
                         });
                         utils.goToPage('registro3');

                } else {
                        utils.alertError('El celular debe ser de 8 dígitos');
                }
            };
            
            $scope.reenviarCodigo = function(){
                if(localStorage.RegistroTelefono){
                    $scope.enviarCodigo(localStorage.numeroRegistro);
                }  
            };
            
            $scope.Save3 = function (codigo) {
                if (localStorage.random.toString() === codigo.toString()) {
                        var JSONObject = {"usuario": localStorage.RegistroNombreUsuario, "password": localStorage.RegistroPassword};
                        utils.postHttp("gotapUserLogin", JSONObject).then(function (resolvedVal) {
                            //localStorage.clear();                      
                            localStorage.idUsuario = resolvedVal.data.id;
                            localStorage.nombre = resolvedVal.data.nombre;
                            localStorage.apellido = resolvedVal.data.apellido;
                            localStorage.apellido2 = resolvedVal.data.apellido2;
                            localStorage.identificador = resolvedVal.data.identificador;
                            localStorage.email = resolvedVal.data.email;
                            localStorage.usuario = resolvedVal.data.usuario;
                            localStorage.password = resolvedVal.data.password;
                            localStorage.regisTimestamp = resolvedVal.data.fechaRegistro;
                            localStorage.provincia = resolvedVal.data.provincia;
                            localStorage.canton = resolvedVal.data.canton;
                            localStorage.fechaRegistro = utils.formatDate(resolvedVal.data.fechaRegistro);
                            
                            //guardamos el telefono por defecto
                            var JSONObject = {"numero": localStorage.RegistroTelefono, "alias":"Teléfono Principal","company": " ", "favorito": "si", "idUsuario": localStorage.idUsuario, "password": localStorage.password};
                            utils.postHttp("gotapInsertTelefono", JSONObject).then(function (resolvedVal) {});
                            //};
                            utils.goToPage('registro4');
                        });
                } else {
                    utils.alertError("El código debe ser igual al recibido por SMS");
                }

            };
            
            
            //Validaciones de tarjeta
            $scope.validarTarjeta = function(){
              //TODO CARLOS
              if(1==1){
                  //Si es correcto redirect a bienvenido
                  utils.goToPage('bienvenido');
              }  
            };
            
            //Open url gotap
            $scope.openWeb = function (link) {
                utils.openWeb(link)
                //$window.open('http://www.gotap.com', '_blank', 'location=yes');
            };
            
            //obtenemos el array de cantones en funcion de la provincia
            $scope.getCantones = function (provincia) {

                switch (provincia) {
                    case "San José":
                        $scope.cantones = [{canton: "San José"}, {canton: "Escazú"}, {canton: "Desamparados"}, {canton: "Puriscal"}, {canton: "Tarrazú"}, {canton: "Aserrí"}, {canton: "Mora"}, {canton: "Goicoechea"}, {canton: "Santa Ana"}, {canton: "Alajuelita"}, {canton: "Vásquez de Coronado"}, {canton: "Acosta"}, {canton: "Tibás"}, {canton: "Moravia"}, {canton: "Montes de Oca"}, {canton: "Turrubares"}, {canton: "Dota"}, {canton: "Curridabat"}, {canton: "Pérez Zeledón"}, {canton: "León Cortés Castro"}];
                        break;
                    case "Alajuela":
                        $scope.cantones = [{canton: "Alajuela"}, {canton: "San Ramón"}, {canton: "Grecia"}, {canton: "San Mateo"}, {canton: "Atenas"}, {canton: "Naranjo"}, {canton: "Palmares"}, {canton: "Poás"}, {canton: "Orotina"}, {canton: "San Carlos"}, {canton: "Zarcero"}, {canton: "Valverde Vega"}, {canton: "Upala"}, {canton: "Los Chiles"}, {canton: "Guatuso"}];
                        break;
                    case "Cartago":
                        $scope.cantones = [{canton: "Cartago"}, {canton: "Paraíso"}, {canton: "La unión"}, {canton: "Jimenez"}, {canton: "Turriabla"}, {canton: "Alvarado"}, {canton: "Oreamuno"}, {canton: "El Guarco"}];
                        break;
                    case "Heredia":
                        $scope.cantones = [{canton: "Heredia"}, {canton: "Barva"}, {canton: "Santo Domingo"}, {canton: "Santa Bárbara"}, {canton: "San Rafael"}, {canton: "San Isidro"}, {canton: "Belén"}, {canton: "Flores"}, {canton: "San Pablo"}, {canton: "Sarapiquí"}];
                        break;
                    case "Limón":
                        $scope.cantones = [{canton: "Limón"}, {canton: "Pococí"}, {canton: "Siquirres"}, {canton: "Talamanca"}, {canton: "Matina"}, {canton: "Guácimo"}];
                    case "Guanacaste":
                        $scope.cantones = [{canton: "Puntarenas"}, {canton: "Esparza"}, {canton: "Buenos Aires"}, {canton: "Montes de Oro"}, {canton: "Osa"}, {canton: "Quepos"}, {canton: "Golfito"}, {canton: "Coto Brus"}, {canton: "Parrita"}, {canton: "Corredores"}, {canton: "Garabito"}];
                    case "Puntarenas":
                        $scope.cantones = [{canton: "Liberia"}, {canton: "Nicoya"}, {canton: "Santa Cruz"}, {canton: "Bagaces"}, {canton: "Carrillo"}, {canton: "Cañas"}, {canton: "Abangares"}, {canton: "Tilarán"}, {canton: "Nandayure"}, {canton: "La Cruz"}, {canton: "Hojancha"}];
                        break;
                    default:
                }
            };

            //Código replicado... para hacer un apaño
            $scope.addTarjeta = function (numero, nombre, mes, ano, cvc) {
                var EsValido;
                var tipoTarjeta;
                var añoActual = new Date().getFullYear();
                
                if (!numero) {
                    utils.alertError("Introduce el número de tarjeta");
                } else if (!nombre) {
                    utils.alertError("Introduce el nombre del titular de la tarjeta");
                } else if (!ano) {
                    utils.alertError("Introduce un año de expiración válido");
                } else if (!mes) {
                    utils.alertError("Introduce un mes de expiración válido");
                } else if (!cvc) {
                    utils.alertError("Introduce un cvc válido");

                } else {
                    //Antes de nada con una Funcion de Jquery que implementa el algoritmo de Luhn miramos si la tarjeta es valida : https://github.com/PawelDecowski/jQuery-CreditCardValidator/
                    /*$('#validarTarjeta').validateCreditCard(function (result) {
                        if (result.valid === true && result.length_valid === true && result.luhn_valid === true) {
                            console.log(result.card_type.name);
                            EsValido = true;
                            tipoTarjeta = result.card_type.name;
                        } else {
                            EsValido = false;
                        }
                        });*/
                    //debugger;
                    //modificar
                    EsValido = true;
                    tipoTarjeta = "VISA"
                    console.log(tipoTarjeta);
                    var anyExpiracion = parseInt("20" + ano);

                    if (!EsValido || nombre.length === 0 || mes.length < 2 || ano.length < 2 || cvc.length < 3 || anyExpiracion < añoActual) {
                        utils.alertError("Esta tarjeta no es válida \n ¡Por favor introduce una que lo sea!");
                    } else {
                        var noms = nombre.split(" ");
                        var fechaNueva = new Date(anyExpiracion, mes, 00);
                        console.log(fechaNueva);
                        var fechita = fechaNueva.getFullYear()+"-"+parseInt(fechaNueva.getMonth()+1)+"-"+fechaNueva.getDate();
                        var JSONObject = {"numero": numero, "nombrePropietario": noms[0], "apellidoPropietario": noms[1], "cvc": cvc, "caducidad": fechita, "tipo": tipoTarjeta, "estado": "activa", "idUsuario": localStorage.idUsuario, "password": localStorage.password};
                        console.log(JSONObject);
                        utils.postHttp("gotapInsertTarjeta", JSONObject).then(function (resolvedVal) {
                            debugger;
                            if (resolvedVal.status === 500) {
                                utils.alertError('¡No ha sido posible añadir su tarjeta!');
                            } else {
                                //$scope.relogin(,false)
                                utils.goToPage('bienvenido');
                                //location.reload();
                                
                            }
                        });
                    }
                };
            };

            $scope.getMonth = function (mes) {
                var d = new Date(mes);
                return d.getMonth() + 1;
            };
            
            $scope.getYear = function (año) {
                var d = new Date(año);
                return d.getFullYear();
            };
            
            //código replicado de login
            
             /* Función de login */
            $scope.relogin = function (userName, password, remember) {
                //utils.goToPage('registro');
                console.log(userName, password);
                if(typeof(userName) == "undefined" || typeof(password) == "undefined")
                {
                    utils.alertError('No has introducido el usuario y/o password.');
                    return false;
                }
                
                var JSONObject = {"usuario": userName, "password": password};
                
                   /* $ionicPopup.alert({
                    title: 'Success',
                    content: 'Hello World!!!'
                    }).then(function(res) {
                        console.log('Test Alert Box');
                    });*/
                utils.postHttp("gotapUserLogin", JSONObject).then(function (resolvedVal) {
                    console.log(resolvedVal);
                    if (resolvedVal.status === 500) {
                        utils.alertError('Usuario o contraseña incorrecto.<br> Por favor revísalos.');
                        return false;
                    } else {
                        //This is called when the promise resolves
                        //console.log(resolvedVal);  logs the value the promise resolves to
                        localStorage.idUsuario = resolvedVal.data.id;
                        localStorage.nombre = resolvedVal.data.nombre;
                        localStorage.apellido = resolvedVal.data.apellido;
                        localStorage.apellido2 = resolvedVal.data.apellido2;
                        localStorage.identificador = resolvedVal.data.identificador;
                        localStorage.email = resolvedVal.data.email;
                        localStorage.usuario = resolvedVal.data.usuario;
                        localStorage.password = resolvedVal.data.password;
                        localStorage.regisTimestamp = resolvedVal.data.fechaRegistro;
                        localStorage.provincia = resolvedVal.data.provincia;
                        localStorage.canton = resolvedVal.data.canton;
                        localStorage.fechaRegistro = utils.formatDate(resolvedVal.data.fechaRegistro);
                        
                        //recordar usuario
                        if(remember){
                            sessionStorage.usuario = userName;
                            sessionStorage.password = password;
                        }
                        else{
                            sessionStorage.usuario = "";
                            sessionStorage.password = "";
                        }
                        
                        if (resolvedVal.data.estado === "inactivo") {
                            utils.alertError('El usuario ' + resolvedVal.data.usuario.toUpperCase() + ' se dió de baja. <br> Por favor regístrate de nuevo');
                            localStorage.clear();

                            /*   } else if (resolvedVal.data.uuidDevice !== utils.getUUID()) {
                             console.log(resolvedVal.data.uuidDevice + " - " + utils.getUUID());
                             document.location = '#/verificarNumero';*/
                        } else {

                            var JSONObject2 = {"idUsuario": localStorage.idUsuario, "password": localStorage.password};
                            utils.postHttp("gotapGetTelefonos", JSONObject2).then(function (resolvedVal) {
                                
                                if (resolvedVal.status === 500) {
                                    console.log(resolvedVal.status);
                                } else {
                                    console.log(resolvedVal.data.length);
                                    
                                    if (resolvedVal.data.length!==0) {
                                        localStorage.numeroRegistro = resolvedVal.data[0].numero;
                                        //$state.go('menu.recargaAhora');
                                        utils.goToPage('menu/recargaAhora')
                                    } else {
                                        //cambiar
                                        utils.goToPage('menu/recargaAhora')
                                        //utils.goToPage('validarNumero')
                                        //document.location = '#/validarNumero';
                                    }

                                }
                            });
                        }
                    }
                });
            };


        });



