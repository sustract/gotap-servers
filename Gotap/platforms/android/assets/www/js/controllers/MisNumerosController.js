/*
 * Autor @CarloOSX
 */
/*Definimos el modulo al que pertenecemos*/
angular.module('Gotap')
//My Class
        /*Definimos el controlador que vamos a usar*/
        .controller('MisNumerosController', function (IonicFunctions, /*Starting with Angular injection*/ $scope, /* nfcService,*/ $filter, $ionicModal, $ionicHistory) {
            /**
             * Variables globales y objetos
             */
            /*declaramos el objeto ionic con el que podremos acceder a los "métodos" definidas en su clase*/
            utils = new IonicFunctions();
            $scope.utils = utils;
            var JSONObject = {"idUsuario": localStorage.idUsuario, "password": localStorage.password};
            utils.postHttp("gotapGetTelefonos", JSONObject).then(function (resolvedVal) {
                if (resolvedVal.status === 500) {
                    console.log(resolvedVal.status);
                } else {
                    $scope.telefonos = resolvedVal.data;
                }
            });
           
           console.log($scope.telefonos);
           
            //Inicializamos nombre y telefono para la modal
            $scope.Nombre = localStorage.nombre;
            $scope.Telefono = localStorage.numeroRegistro;
            
            //Vuelve a la pantalla anterior de navegación
            $scope.goBack = function(){
                $ionicHistory.backView().go();                    
            };
            
            //Mostrar menu gotap
            $scope.showGotapMenu = {modal:null};
            
            $scope.showGotapMenu = function(){
                   //debugger;
                $ionicModal.fromTemplateUrl('templates/modal-gotapmenu.html', {
                        scope: $scope,
                        animation: 'slide-in-up',
                        focusFirstInput: true
                }).then(function(modal) {
                        $scope.showGotapMenu.modal = modal;
                        $scope.showGotapMenu.openModal();
                });
                $scope.showGotapMenu.openModal = function() {
                        angular.element(document.querySelector('#nav-view-parent')).addClass('blur');
                        $scope.showGotapMenu.modal.show();
                };
                $scope.showGotapMenu.closeModal = function() {
                        $scope.showGotapMenu.modal.hide();
                };
                $scope.$on('modal.hidden', function() {
                        angular.element(document.querySelector('#nav-view-parent')).removeClass('blur');
                });
                $scope.goTo = function (pantalla){
                        $scope.showGotapMenu.closeModal();
                        utils.goToPage(pantalla);  
                };
            };
            
           $scope.deleteTelefono = function (numero) {
                var telefono =  $scope.telefonos.filter(function (telefono) { return telefono.numero === numero; });   
                        var JSONObject = {"id": telefono[0].id, "numero": parseInt(telefono[0].numero), "alias":telefono[0].alias,"company": "kolbi", "estado": "inactivo", "idUsuario": localStorage.idUsuario, "password": localStorage.password};
                        utils.postHttp("gotapUpdateTelefono", JSONObject).then(function (resolvedVal) {
                        if (resolvedVal.status === 500) {
                                utils.alertError('Error eliminando telefono');
                        } else {
                        location.reload();
                        }
                        });
            };
            
           $scope.addTelefono = function (movil, alias) {   
                var JSONObject = {"numero": movil, "alias":alias,"company": " ", "favorito": "si", "idUsuario": localStorage.idUsuario, "password": localStorage.password};
                utils.postHttp("gotapInsertTelefono", JSONObject).then(function (resolvedVal) {
                    if (resolvedVal.status === 500) {
                        utils.alertError('Error al añadir telefono telefono');
                    } else {
                       location.reload();
                    }
                });
            };
        });