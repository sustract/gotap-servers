/*
 * Autor @CarloOSX
 */


/*Definimos el modulo al que pertenecemos*/
angular.module('Gotap')
//My Class
        /*Definimos el controlador que vamos a usar*/
        .controller('ValidarNumeroController', function (IonicFunctions, $scope, $http, $ionicPopup, $timeout, $cordovaVibration, $ionicLoading, $cordovaDevice, $state) {
            /**
             * Variables globales y objetos
             */

            /*declaramos el objeto ionic con el que podremos acceder a los "métodos" definidas en su clase*/
            utils = new IonicFunctions();
            $scope.utils = utils;            

            var JSONObject = {"idUsuario": localStorage.idUsuario, "password": localStorage.password};
            utils.postHttp("gotapGetTelefonos", JSONObject).then(function (resolvedVal) {

                if (resolvedVal.status === 500) {

                    utils.alertError("La validación no puede llevarse a cabo en este momento <br> Por favor inténtalo de nuevo más tarde");
                } else {

                    $scope.telefonoRegistro = resolvedVal.data[0].numero;
                }
            });
            var globalRandom = "0";
            $scope.SendPhoneRandom = function (phoneNumber) {

                localStorage.numeroRegistro = phoneNumber;
                randomSix = Math.floor(100000 + Math.random() * 900000);
                globalRandom = randomSix;
                if (phoneNumber) {
                    numero = phoneNumber.toString();
                    if (numero.length === 9) {
                        $http.get("http://appsrv01.sevenandseven.net:28082/SendSMS/+34" + numero + "/" + randomSix.toString()).
                                then(function (response) {
                                }, function (response) {
                                    utils.alertError('¡Error al enviar SMS!');
                                });
                        $ionicLoading.show({
                            template: 'Enviando...',
                            duration: 4000
                        });
                    } else if (numero.length === 8) {
                        $http.get("http://appsrv01.sevenandseven.net:28082/SendSMS/+506" + numero + "/" + randomSix.toString()).
                                then(function (response) {
                                }, function (response) {
                                    utils.alertError('¡Error al enviar SMS!');
                                });
                        $ionicLoading.show({
                            template: 'Enviando...',
                            duration: 4000
                        });
                    } else {
                        utils.alertError('El número debe tener 8 dígitos');
                    }
                } else {

                    utils.alertError('¡El número es obligatorio!');
                }

            };
            $scope.VerifyAndSend = function (phoneNumber) {
                randomSix = Math.floor(100000 + Math.random() * 900000);
                globalRandom = randomSix;
                if (phoneNumber) {
                    numero = phoneNumber.toString();
                    numeroRegistro = $scope.telefonoRegistro.toString();
                    if (numeroRegistro === numero) {
                        if (numero.length === 9) {
                            $http.get("http://appsrv01.sevenandseven.net:28082/SendSMS/+34" + numero + "/" + randomSix.toString()).
                                    then(function (response) {
                                    }, function (response) {
                                        utils.alertError('¡Error al enviar SMS!');
                                    });
                            $ionicLoading.show({
                                template: 'Enviando...',
                                duration: 4000
                            });
                        } else if (numero.length === 8) {
                            $http.get("http://appsrv01.sevenandseven.net:28082/SendSMS/+506" + numero + "/" + randomSix.toString()).
                                    then(function (response) {
                                    }, function (response) {
                                        utils.alertError('¡Error al enviar SMS!');
                                    });
                            $ionicLoading.show({
                                template: 'Enviando...',
                                duration: 4000
                            });
                        } else {
                            utils.alertError('El número debe tener 8 dígitos');
                        }
                    } else {
                        utils.alertError('¡El Número debe ser el mismo con el que se registró');
                    }
                } else {
                    utils.alertError('¡El número es obligatorio!');
                }
            };
            $scope.ValidatePhoneRandom = function (codeNumber) {

                if (globalRandom === codeNumber) {

              var JSONObject = {"numero": localStorage.numeroRegistro, "alias":"Mi número","company": " ", "favorito": "si", "idUsuario": localStorage.idUsuario, "password": localStorage.password};
        
                utils.postHttp("gotapInsertTelefono", JSONObject).then(function (resolvedVal) {

                        if (resolvedVal.status === 500) {

                            utils.alertError("La verificacion no puede llevarse a cabo en este momento <br> Por favor inténtalo de nuevo más tarde");

                        } else {
                            console.log(resolvedVal);
                            utils.goToPage('menu/recargaAhora')
                            //$state.go('menu.recargaAhora');
                        }
                    });

                } else {
                    if (codeNumber) {
                        codeNumber = codeNumber.toString();
                        if (codeNumber.length === 6) {
                            utils.alertError("El código no es correcto");
                        } else {
                            utils.alertError("El código debe tener 6 dígitos");
                        }
                    } else {
                        utils.alertError("El código no puede estar vacío");
                    }
                }
            };
            $scope.ValidateAndUpdate = function (codeNumber) {
                 
                var uuid = "null";
                uuid = utils.getUUID();

                if (globalRandom === codeNumber) {

                    var JSONObject;
                    if (!$scope.userPass) {
                        JSONObject = {"idUsuario": localStorage.idUsuario, "nombre": localStorage.nombre, "apellido": localStorage.apellido, "apellido2": localStorage.apellido2, "identificador": localStorage.identificador, "email": localStorage.email, "usuario": localStorage.usuario, "password": localStorage.password, "passwordUser": localStorage.password, "fechaRegistro": localStorage.regisTimestamp, "estado": "activo", "uuidDevice": utils.getUUID(), "pais": "Costa Rica", "provinciaEstado": localStorage.provinciaEstado, "cantonCiudad": localStorage.cantonCiudad, "direccion": "", "codigoPostal": ""};
                    }

                    utils.postHttp("gotapUserChange", JSONObject).then(function (resolvedVal) {
                         console.log(resolvedVal);

                        if (resolvedVal.status === 500) {

                            utils.alertError("La validación no puede llevarse a cabo en este momento <br> Por favor inténtalo de nuevo más tarde");
                        } else {

                            setTimeout( utils.goToPage('menu/recargaAhora')/*$state.go('menu.recargaAhora')*/, 3000);
                        }
                    });

                } else {
                    if (codeNumber) {
                        codeNumber = codeNumber.toString();
                        if (codeNumber.length === 6) {
                            utils.alertError("¡El código no es correcto!");
                        } else {
                            utils.alertError("¡El código debe tener 6 dígitos!");
                        }
                    } else {
                        utils.alertError("¡El código no puede estar vacío!");
                    }
                }
            };
        });