/*
 * Autor @CarloOSX
 */
/*Definimos el modulo al que pertenecemos*/
angular.module('Gotap')
//My Class
        /*Definimos el controlador que vamos a usar*/
        .controller('TransaccionesController', function (IonicFunctions, /*Starting with Angular injection*/ $scope, /* nfcService,*/ $filter, $ionicModal, $ionicHistory) {
            /**
             * Variables globales y objetos
             */
            /*declaramos el objeto ionic con el que podremos acceder a los "métodos" definidas en su clase*/
            utils = new IonicFunctions();
            $scope.utils = utils;  
            
            //Inicializamos nombre y telefono para la modal
            $scope.Nombre = localStorage.nombre;
            $scope.Telefono = localStorage.numeroRegistro;
            
            //Vuelve a la pantalla anterior de navegación
            $scope.goBack = function(){
                $ionicHistory.backView().go();                    
            };          
            
            var JSONObject = {"idUsuario": localStorage.idUsuario, "password": localStorage.password};
            utils.postHttp("gotapGetTransactions", JSONObject).then(function (resolvedVal) {
                if (resolvedVal.status === 500) {
                    console.log(resolvedVal.status);
                } else {
                    console.log(resolvedVal.data);
                    $scope.transactions = resolvedVal.data;
                }
            });
            
            $scope.getTransactions = function(){
                var JSONObject = {"idUsuario": localStorage.idUsuario, "password": localStorage.password};
                utils.postHttp("gotapGetTransactions", JSONObject).then(function (resolvedVal) {
                    if (resolvedVal.status === 500) {
                        console.log(resolvedVal.status);
                    } else {
                        console.log(resolvedVal.data);
                        $scope.transactions = resolvedVal.data;
                    }
                });
            };

            $scope.formatDate = function (fecha) {
                if (fecha) {
                    Number.prototype.padLeft = function (base, chr) {
                        var len = (String(base || 10).length - String(this).length) + 1;
                        return len > 0 ? new Array(len).join(chr || '0') + this : this;
                    };
                    var d = new Date(fecha),
                            dformat = [
                                d.getDate().padLeft(),
                                (d.getMonth() + 1).padLeft(),
                                d.getFullYear()].join('/') +
                            ' ' +
                            [d.getHours().padLeft(),
                                d.getMinutes().padLeft(),
                                d.getSeconds().padLeft()].join(':');
                    return dformat;
                }
            };
            
            //Mostrar menu gotap
            $scope.showGotapMenu = {modal:null};
            
            $scope.showGotapMenu = function(){
                   //debugger;
                $ionicModal.fromTemplateUrl('templates/modal-gotapmenu.html', {
                        scope: $scope,
                        animation: 'slide-in-up',
                        focusFirstInput: true
                }).then(function(modal) {
                        $scope.showGotapMenu.modal = modal;
                        $scope.showGotapMenu.openModal();
                });
                $scope.showGotapMenu.openModal = function() {
                        angular.element(document.querySelector('#nav-view-parent')).addClass('blur');
                        $scope.showGotapMenu.modal.show();
                };
                $scope.showGotapMenu.closeModal = function() {
                        $scope.showGotapMenu.modal.hide();
                };
                $scope.$on('modal.hidden', function() {
                        angular.element(document.querySelector('#nav-view-parent')).removeClass('blur');
                });
                $scope.goTo = function (pantalla){
                        $scope.showGotapMenu.closeModal();
                        utils.goToPage(pantalla);  
                };
            };
        });