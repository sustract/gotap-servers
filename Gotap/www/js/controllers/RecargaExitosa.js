/*
 * Autor @CarloOSX
 */

/* global Function, constructor, angular para poder hacer extends si es necesario */

Function.prototype.extends = function (superclass) {
    var constructor = this.prototype.constructor;
    this.prototype = Object.create(superclass.prototype);
    this.prototype.constructor = constructor;
    return this;
};

/*Definimos el modulo al que pertenecemos*/
angular.module('Gotap')
//My Class
        /*Definimos el controlador que vamos a usar*/
        .controller('RecargaExitosa', function (IonicFunctions, /*Starting with Angular injection*/ $scope, $state, $rootScope, $ionicPopup, $timeout, $cordovaVibration, $ionicScrollDelegate, $ionicSlideBoxDelegate, $window) {
            /**
             * Variables globales y objetos
             */

            /*declaramos el objeto ionic con el que podremos acceder a los "métodos" definidas en su clase*/
            utils = new IonicFunctions();
            $scope.utils = utils;            

            $scope.$on('$ionicView.enter', function(e) {
                $timeout(function() {
                    //goToPage('recargarAhora');
                    utils.goToPage('menu/recargaAhora')
                     //$state.go('menu.recargaAhora');
                }, 3000);
            });   
           
            //Open url gotap
            $scope.openWeb = function(){
                $window.open('http://www.gotap.com', '_blank', 'location=yes');
            };
        });
