/*
 * Autor @CarloOSX
 */
/*Definimos el modulo al que pertenecemos*/
angular.module('Gotap')
//My Class
        /*Definimos el controlador que vamos a usar*/
        .controller('RecargaAhoraController', function (IonicFunctions, /*Starting with Angular injection*/ $scope, $ionicLoading, nfcService, $filter, $ionicModal, $ionicHistory) {
            /**
             * Variables globales y objetos
             */
             $scope.$on('$ionicView.beforeEnter', function(e) {
                //Inicializamos nombre y telefono para la modal
                $scope.Nombre = localStorage.nombre;
                $scope.Telefono = localStorage.numeroRegistro;
                
                $scope.tag = nfcService.tag;
           
                $scope.clear();
                
                $scope.tarjetas = null;
                $scope.telefonos = null;
                $scope.InitData();
             });   
             
            $scope.clear = function () {
                nfcService.clearTag();
            };
            
            //Vuelve a la pantalla anterior de navegación
            $scope.goBack = function(){
                $ionicHistory.backView().go();                    
            };

            /*declaramos el objeto ionic con el que podremos acceder a los "métodos" definidas en su clase*/
            utils = new IonicFunctions();
            $scope.utils = utils;

            $scope.InitData = function(){
                var JSONObject = {"idUsuario": localStorage.idUsuario, "password": localStorage.password};
                utils.postHttp("gotapGetTarjetas", JSONObject).then(function (resolvedVal) {
                        //debugger;
                        if (resolvedVal.status === 500) {
                        //utils.alertError("Error al importar tarjetas");
                        } else {
                        if (resolvedVal.data.length > 0) {
                                var tarjetas = resolvedVal.data;
                                for (var i = 0; i < tarjetas.length; i++) {
                                if (tarjetas[i].estado === "activa") {
                                        tarjetas[i].numero = (tarjetas[i].numero).toString();
                                }
                                }
                                $scope.tarjetas = resolvedVal.data;
                        } else if (resolvedVal.data.length === 0) {
                                //utils.alertError('Primero debes añadir una tarjeta');
                        }
                        }
                });
        
                utils.postHttp("gotapGetTelefonos", JSONObject).then(function (resolvedVal) {
                        if (resolvedVal.status === 500) {
                        //utils.alertError("Error al importar teléfonos");
                        } else {
                        $scope.telefonos = resolvedVal.data;
                        }
                });
            };
            
            //Mostrar menu gotap
            $scope.showGotapMenu = {modal:null};
            
            $scope.showGotapMenu = function(){
                   //debugger;
                $ionicModal.fromTemplateUrl('templates/modal-gotapmenu.html', {
                        scope: $scope,
                        animation: 'slide-in-up',
                        focusFirstInput: true
                }).then(function(modal) {
                        $scope.showGotapMenu.modal = modal;
                        $scope.showGotapMenu.openModal();
                });
                $scope.showGotapMenu.openModal = function() {
                        angular.element(document.querySelector('#nav-view-parent')).addClass('blur');
                        $scope.showGotapMenu.modal.show();
                };
                $scope.showGotapMenu.closeModal = function() {
                        $scope.showGotapMenu.modal.hide();
                };
                $scope.$on('modal.hidden', function() {
                        angular.element(document.querySelector('#nav-view-parent')).removeClass('blur');
                });
                $scope.goTo = function (pantalla){
                        $scope.showGotapMenu.closeModal();
                        utils.goToPage(pantalla);  
                };
            };

            $scope.showTag = function () {
                    
                $ionicLoading.show({
                        template: 'Procesando Transacción... <br> Esta operación puede tardar unos segundos ',
                        duration: 15000
                });
                
                var numero = $("#tarjMarcado option:selected").text();
                var telefonoA = $("#telfMarcado option:selected").text();
                
                var tarjeta = $scope.tarjetas.filter(function (tarjeta) {
                    return tarjeta.numero === numero;
                });
                
                var telefono = $scope.telefonos.filter(function (telefono) {
                    return telefono.alias === telefonoA;
                });

                var valor = $filter('bytesToHexString')($scope.tag.id);
                console.log(JSON.stringify(tarjeta));
                
                var d = new Date(tarjeta[0].caducidad);
                //var d = new Date();
                var años = d.getFullYear().toString().substring(2);
                var mesi = d.getMonth() + 1;
                var mes = 0;

                switch (mesi) {
                    case 1:
                        mes = "01";
                        break;
                    case 2:
                        mes = "02";
                        break;
                    case 3:
                        mes = "03";
                        break;
                    case 4:
                        mes = "04";
                        break;
                    case 5:
                        mes = "05";
                        break;
                    case 6:
                        mes = "06";
                        break;
                    case 7:
                        mes = "07";
                        break;
                    case 8:
                        mes = "08";
                        break;
                    case 9:
                        mes = "09";
                        break;
                    case 10:
                        mes = "10";
                        break;
                    case 11:
                        mes = "11";
                        break;
                    case 12:
                        mes = "12";
                        break;
                };

                console.log(años);
                console.log(mes.toString());

                var JSONObject3 = {"storeID": "4ae53f41-cca9-4cf5-8bc7-8c7046be4b5a", "storePwd": "4b5976e57fb64d969e228be7ad232a1f", "holderName": tarjeta[0].nombrePropietario, "holderLastName": tarjeta[0].apellidoPropietario, "holderID": localStorage.identificador, "NFCID": 1, "holderPhone": "506" + telefono[0].numero, "ccnumber": tarjeta[0].numero, "expmonth": mes.toString(), "expyear": años, "cvv": tarjeta[0].cvc.toString(), "orderid": "TEST-TRANSACTION", "idUsuario": localStorage.idUsuario, "password": localStorage.password, "localizacion": "No disponible", "tag": valor};
                
                /*var JSONObject3 = {"storeID": "4ae53f41-cca9-4cf5-8bc7-8c7046be4b5a", "storePwd": "4b5976e57fb64d969e228be7ad232a1f", "holderName": "SUS NOMBRE", "holderLastName": "SUS APELLIDO", "holderID": localStorage.identificador, "NFCID": 1, "holderPhone": "506" + 646200337, "ccnumber": 666, "expmonth": mes.toString(), "expyear": años, "cvv": 353, "orderid": "TEST-TRANSACTION", "idUsuario": localStorage.idUsuario, "password": localStorage.password, "localizacion": "No disponible", "tag": valor};*/

                console.log(JSONObject3);
                utils.postHttp("gotapAddTransaction", JSONObject3).then(function (resolvedVal) {
                    if (resolvedVal.status === 500) {
                        utils.alertError("Error al realizar la transaccion");
                        nfcService.clearTag();
                    } else {
                        if (resolvedVal.data === "La transaccion no ha podido ser añadida") {
                            $ionicLoading.hide();
                            utils.alertError("La transaccion no ha podido ser añadida");
                            nfcService.clearTag();
                        } else {
                            $ionicLoading.hide();
                            //utils.alertError("Operación :" + resolvedVal.data.Request.Transaction.Recarga.status + "<br>" + " Tag utilizado: " + valor + "<br>" + " Importe Recargado: " + resolvedVal.data.Request.Transaction.tarjeta.amount + " Colones");
                            nfcService.clearTag();
                            utils.goToPage('recargaExitosa');  
                        }
                    }
                });
            };
        });
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
/*
 * 
 * 
 * 
 
 
 <!-- contenido del NFC -->
 <div ng-if="tag.id">
 <div class="card">
 <div class="item item-text-wrap">
 <dl>
 <dt>Tag Id</dt>
 <dd>{{ tag.id | bytesToHexString }}</dd>
 <dt>Tag Type</dt>
 <dd>{{ tag.type}}</dd>
 <!-- TODO techTypes -->
 <dt>Max Size</dt>
 <dd>{{ tag.maxSize}} bytes</dd>
 <dt>Is Writable</dt>
 <dd>{{ tag.isWritable}}</dd>
 <dt>Can Make Read Only</dt>
 <dd>{{ tag.canMakeReadOnly}}</dd>
 </dl>
 <div class="card" ng-repeat="record in tag.ndefMessage">
 <div class="item item-divider">
 Record {{$index + 1}} <br/>
 </div>
 <div class="item item-text-wrap">
 <dl>
 <dt>TNF</dt>
 <dd>{{ record.tnf | tnfToString }}</dd>
 <dt>Record Type</dt>
 <dd>{{ record.type | bytesToString }}</dd>
 <dt ng-if="record.id">Record Id</dt>
 <dd ng-if="record.id">{{ record.id | bytesToString }}</dd>
 <dt>Payload</dt>
 <dd>{{ record | decodePayload }}</dd>
 </dl>
 </div>
 </div>
 </div>
 </div>
 </div>
 <!-- / contenido del NFC -->
 
 
 * 
 * 
 * 
 */