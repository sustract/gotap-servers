/*
 * Autor @CarloOSX
 */

/* global Function, constructor, angular para poder hacer extends si es necesario */

Function.prototype.extends = function (superclass) {
    var constructor = this.prototype.constructor;
    this.prototype = Object.create(superclass.prototype);
    this.prototype.constructor = constructor;
    return this;
};

/*Definimos el modulo al que pertenecemos*/
angular.module('Gotap')
//My Class
        /*Definimos el controlador que vamos a usar*/
        .controller('MapaController', function (IonicFunctions, /*Starting with Angular injection*/ $scope, $cordovaGeolocation, $state, $rootScope, $ionicPopup, $timeout, $cordovaVibration, $ionicScrollDelegate, $ionicSlideBoxDelegate, $window, $ionicHistory, $ionicLoading) {
                $scope.currentLocation;
                $scope.showLocation = false;
                
                //Vuelve a la pantalla anterior de navegación
                $scope.goBack = function(){
                    $ionicHistory.backView().go();                    
                };
                    
                $scope.$on('$ionicView.enter', function(e) {
                    //Només ens geolocalitzem si no tenim current location, després la mantenim
                    if(typeof($scope.currentLocation) == "undefined"){
                        hereIAM();  
                    }
                });  
                              
                $scope.buttonHere = function(current){
                    var options = {timeout: 100, enableHighAccuracy: true};
                    var latLng = {};
                    
                    $ionicLoading.show({
                        content: 'Buscando carteles...',
                        showBackdrop: false
                    });

                    if(typeof(current) != "undefined"){
                        latLng = current;
                        setMap(latLng, false);
                    }else{
                            $cordovaGeolocation.getCurrentPosition(options).then(function(position){
                                latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                                $scope.currentLocation = latLng;
                                setMap(latLng, true);
                            }, function(error){
                                $ionicPopup.confirm({
                                    title: "Localitzación fallida",
                                    template: "Comprueba que tienes connexión y vuelve a probar.",
                                    buttons: [
                                    { text: "Aceptar", type: 'button button-block button-positive', onTap: function(e) { } }
                                            ]
                                 });
                            //console.log("Could not get location");
                        });  
                    }
                    $ionicLoading.hide();  
                };
                
                var setMap = function(latLng, redirect){
                var mapOptions = {
                        center:latLng,
                        zoom: 17,
                        mapTypeId: google.maps.MapTypeId.SATELLITE,
                        controls:{
                            compass:true,
                            myLocationButton:true
                        },
                        gestures:{
                            scroll:true,
                            rotate:true,
                            zoom:true
                        }
                    };
                            
                    $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);       
                        var marker = new google.maps.Marker({
                            map: $scope.map,
                            animation: google.maps.Animation.DROP,
                            position: latLng
                        });           
                        var infoWindow = new google.maps.InfoWindow({
                            content: "Estás aquí!"
                        });
                        google.maps.event.addListener(marker, 'click', function () {
                            infoWindow.open($scope.map, marker);
                    });
                    
                    $ionicPopup.confirm({
                        title: "Localitzación con éxito",
                        template: "Coordenadas: "+ $scope.currentLocation,
                        buttons: [
                        { text: "Ok", type: 'button button-block button-positive', onTap: function(e) 
                            {
                            }
                        }
                        ]
                    });
                    
                    $scope.showLocation = true;
                };
                
                var hereIAM = function(current){
                    var latLng = new google.maps.LatLng(41.407478, 2.194575);
                    $scope.currentLocation = latLng;
                    
                    var mapOptions = {
                        center:latLng,
                        zoom: 17,
                        mapTypeId: google.maps.MapTypeId.SATELLITE,
                        controls:{
                        compass:true,
                        myLocationButton:true
                        },
                        gestures:{
                        scroll:true,
                        rotate:true,
                        zoom:true
                        }
                    };
                
                    $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);
                            
                    var marker = new google.maps.Marker({
                        map: $scope.map,
                        animation: google.maps.Animation.DROP,
                        position: latLng
                    });      
                        
                    var infoWindow = new google.maps.InfoWindow({
                        content: "UOC!"
                    });
                        
                    google.maps.event.addListener(marker, 'click', function () {
                        infoWindow.open($scope.map, marker);
                    });
               };
});
