// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'


angular.module('Gotap', ['ionic', 'ngCordova', 'nfcFilters'])
        .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
            $urlRouterProvider.otherwise('/');
            $ionicConfigProvider.views.swipeBackEnabled(false);

            $stateProvider
                    .state('login', {
                        url: '/',
                        templateUrl: 'templates/login.html',
                        controller: 'LoginController'

                    })
                    .state('registro', {
                        url: '/registro',
                        templateUrl: 'templates/registro.html',
                        controller: 'RegistroController'

                    })
                    .state('registro2', {
                        url: '/registro2',
                        templateUrl: 'templates/registro2.html',
                        controller: 'RegistroController'

                    })
                    .state('registro3', {
                        url: '/registro3',
                        templateUrl: 'templates/registro3.html',
                        controller: 'RegistroController'

                    })
                    .state('registro4', {
                        url: '/registro4',
                        templateUrl: 'templates/registro4.html',
                        controller: 'RegistroController'
                    })
                    .state('recargaExitosa', {
                        url: '/recargaExitosa',
                        templateUrl: 'templates/recargaExitosa.html',
                        controller: 'RecargaExitosa'
                    })
                    .state('validarNumero', {
                        url: '/validarNumero',
                        templateUrl: 'templates/validarNumero.html',
                        controller: 'ValidarNumeroController'

                    })
                    .state('verificarNumero', {
                        url: '/verificarNumero',
                        templateUrl: 'templates/verificarNumero.html',
                        controller: 'ValidarNumeroController'

                    })
                    .state('bienvenido', {
                        url: '/bienvenido',
                        templateUrl: 'templates/bienvenido.html',
                        controller: 'BienvenidoController'
                    })
                    .state('menu', {
                        url: '/menu',
                        templateUrl: 'templates/menu.html',
                        abstract: true
                    })
                    .state('menu.recargaAhora', {
                        url: '/recargaAhora',
                        templateUrl: 'templates/menu.html',
                        views: {
                            'recargaAhora': {
                                templateUrl: 'templates/recargaAhora.html',
                                controller: 'RecargaAhoraController'
                            }
                        }
                    })
                    .state('menu.misDatos', {
                        url: '/misDatos',
                        templateUrl: 'templates/misDatos.html',
                        views: {
                            'recargaAhora': {
                                templateUrl: 'templates/misDatos.html',
                                controller: 'MisDatosController'
                            }
                        }

                    })
                    .state('menu.configuracion', {
                        url: '/configuracion',
                        templateUrl: 'templates/configuracion.html',
                        views: {
                            'recargaAhora': {
                                templateUrl: 'templates/configuracion.html',
                                controller: 'MisDatosController'
                            }
                        }

                    })
                    .state('menu.historial', {
                        url: '/historial',
                        templateUrl: 'templates/historial.html',
                        views: {
                            'recargaAhora': {
                                templateUrl: 'templates/historial.html',
                                controller: 'TransaccionesController'
                            }
                        }

                    })
                    .state('menu.cambiarClave', {
                        url: '/cambiarClave',
                        templateUrl: 'templates/cambiarClave.html',
                        views: {
                            'recargaAhora': {
                                templateUrl: 'templates/cambiarClave.html',
                                controller: 'MisDatosController'
                            }
                        }

                    })
                    .state('menu.ayuda', {
                        url: '/ayuda',
                        templateUrl: 'templates/ayuda.html',
                        views: {
                            'recargaAhora': {
                                templateUrl: 'templates/ayuda.html',
                                controller: 'MisDatosController'
                            }
                        }

                    })
                    .state('menu.mapa', {
                        url: '/mapa',
                        templateUrl: 'templates/mapa.html',
                        views: {
                            'recargaAhora': {
                                templateUrl: 'templates/mapa.html',
                                controller: 'MapaController'
                            }
                        }

                    })
                    .state('menu.mediosDePago', {
                        url: '/mediosDePago',
                        templateUrl: 'templates/mediosDePago.html',
                        views: {
                            'recargaAhora': {
                                templateUrl: 'templates/mediosDePago.html',
                                controller: 'MediosDePagoController'
                            }
                        }

                    })
                    .state('menu.misNumeros', {
                        url: '/misNumeros',
                        templateUrl: 'templates/misNumeros.html',
                        views: {
                            'recargaAhora': {
                                templateUrl: 'templates/misNumeros.html',
                                controller: 'MediosDePagoController'
                            }
                        }

                    }) 
                    .state('menu.transacciones', {
                        url: '/transacciones',
                        templateUrl: 'templates/transacciones.html',
                        views: {
                            'recargaAhora': {
                                templateUrl: 'templates/transacciones.html',
                                controller: 'TransaccionesController'
                            }
                        }

                    });
        })

       .factory('nfcService', function ($rootScope, $ionicPlatform) {

            var tag = {};

            $ionicPlatform.ready(function () {
                if(window.navigator && window.navigator.splashscreen) {
                        window.plugins.orientationLock.unlock();
                }
                
                if(typeof(nfc) != "undefined"){
                        nfc.addNdefListener(function (nfcEvent) {
                                console.log(JSON.stringify(nfcEvent.tag, null, 4));
                                $rootScope.$apply(function () {
                                        angular.copy(nfcEvent.tag, tag);
                                });
                        }, function () {
                                console.log("Listening for NDEF Tags.");
                        }, function (reason) {
                                alert("Error adding NFC Listener " + reason);
                                return false;
                        });
                }else{return false;}

            });

            return {
                tag: tag,
                clearTag: function () {
                    angular.copy({}, this.tag);
                }
            };
            
            //return false;
        });



